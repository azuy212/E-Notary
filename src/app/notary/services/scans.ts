import { API, Storage, graphqlOperation } from 'aws-amplify';

import { IRawScanData } from '@/model/scan';
import * as queries from '@/app/notary/graphql/queries';
import * as mutations from '@/app/notary/graphql/mutations';
import {
  CreateDocumentInput,
  ListDocumentsQuery,
  DeleteDocumentInput,
} from '@/app/notary/model/API';

export async function getScansFromCloud(): Promise<IRawScanData[]> {
  const list = (await API.graphql(graphqlOperation(queries.listDocuments))) as {
    data: ListDocumentsQuery;
  };
  if (list.data.listDocuments) {
    const apiResponse = list.data.listDocuments.items;
    if (apiResponse) {
      const rawScanData = apiResponse.map<IRawScanData>(data => ({
        id: data!.id,
        key: data!.documentName,
        lastModified: data!.dateTime,
        size: data!.size!,
      }));
      return rawScanData.sort(
        (a, b) =>
          new Date(b.lastModified).getTime() -
          new Date(a.lastModified).getTime(),
      );
    }
  }
  return [];
}

export async function pushScanToCloud(
  label: string,
  imageURI: string,
  userEmail: string,
  progressCallback: (progress: { loaded: number; total: number }) => void,
): Promise<IRawScanData> {
  try {
    const blob: any = await getImageBlob(imageURI);

    const scanData: IRawScanData = {
      key: label,
      lastModified: new Date().toUTCString(),
      eTag: '',
      size: blob.size,
    };

    await Storage.put(label, blob, {
      progressCallback,
      contentType: blob.type,
    });

    await storeInDB(label, blob.size, userEmail);

    return scanData;
  } catch (error) {
    throw error;
  }
}

export async function deleteScanFromCloud(scan: IRawScanData) {
  try {
    await Storage.remove(scan.key);

    await deleteFromDB(scan.id!);
  } catch (error) {
    throw error;
  }
}

async function getImageBlob(imageURI: string) {
  return await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function () {
      resolve(xhr.response);
    };
    xhr.onerror = function () {
      reject(new TypeError('Network request failed'));
    };
    xhr.responseType = 'blob';
    xhr.open('GET', imageURI, true);
    xhr.send(null);
  });
}

async function storeInDB(label: string, size: number, userEmail: string) {
  const documentDetail: CreateDocumentInput = {
    size,
    userEmail,
    documentName: label,
    dateTime: new Date().toUTCString(),
  };
  const newDocument = await API.graphql(
    graphqlOperation(mutations.createDocument, { input: documentDetail }),
  );
  return newDocument;
}

async function deleteFromDB(id: string) {
  const documentDetail: DeleteDocumentInput = { id };
  await API.graphql(
    graphqlOperation(mutations.deleteDocument, { input: documentDetail }),
  );
}
