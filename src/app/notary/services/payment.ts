import { Alert } from 'react-native';

export const STRIPE_PUBLISHABLE_KEY = 'pk_test_hBCTN2V6KSVZLhCNv63rJYxV00O8e6Fuau';

export const getCreditCardToken = async (creditCardData: ICardData): Promise<any> => {
  const card: Record<string, string> = {
    'card[number]': creditCardData.values.number.replace(/ /g, ''),
    'card[exp_month]': creditCardData.values.expiry.split('/')[0],
    'card[exp_year]': creditCardData.values.expiry.split('/')[1],
    'card[cvc]': creditCardData.values.cvc,
  };
  const response = await fetch('https://api.stripe.com/v1/tokens', {
    headers: {
      // Use the correct MIME type for your server
      accept: 'application/json',
      // Use the correct Content Type to send data to Stripe
      'content-type': 'application/x-www-form-urlencoded',
      // Use the Stripe publishable key as Bearer
      authorization: `Bearer ${STRIPE_PUBLISHABLE_KEY}`,
    },
    // Use a proper HTTP method
    method: 'post',
    // Format the credit card data to a string of key-value pairs
    // divided by &
    body: Object.keys(card)
      .map(key => `${key}=${card[key]}`)
      .join('&'),
  });

  const { id: tokenId } = await response.json();
  return tokenId;

};

export const makePaymentAlert = async (amount: number, yes: () => void, no?: () => void) => {
  Alert.alert(
    `Making a Payment of $${(amount / 100).toFixed(2)}`,
    'Are you sure you want to continue?',
    [
      {
        text: 'Cancel',
        onPress: no,
        style: 'cancel',
      },
      { text: 'OK', onPress: yes },
    ],
    { cancelable: false },
  );
};

export const makePayment = async (tokenId: string, amount: number) => {
  const response = await fetch('https://pk8v6dpt67.execute-api.us-east-1.amazonaws.com/dev/payment', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      tokenId,
      amount,
      currency: 'usd',
    }),
  });
  if (response.status >= 200 && response.status <= 299) {
    return response.json();
  }
  const header = response.headers.get('content-type');
  if (header && header.includes('application/json')) {
    throw await response.json();
  }
  throw new Error(response.statusText);

};
