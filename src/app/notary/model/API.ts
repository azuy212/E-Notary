/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type CreateDocumentInput = {
  id?: string | null,
  documentName: string,
  dateTime: string,
  size?: number | null,
  userEmail: string,
};

export type UpdateDocumentInput = {
  id: string,
  documentName?: string | null,
  dateTime?: string | null,
  size?: number | null,
  userEmail?: string | null,
};

export type DeleteDocumentInput = {
  id?: string | null,
};

export type ModelDocumentFilterInput = {
  id?: ModelIDFilterInput | null,
  documentName?: ModelStringFilterInput | null,
  dateTime?: ModelStringFilterInput | null,
  size?: ModelIntFilterInput | null,
  userEmail?: ModelStringFilterInput | null,
  and?: Array< ModelDocumentFilterInput | null > | null,
  or?: Array< ModelDocumentFilterInput | null > | null,
  not?: ModelDocumentFilterInput | null,
};

export type ModelIDFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelStringFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelIntFilterInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  contains?: number | null,
  notContains?: number | null,
  between?: Array< number | null > | null,
};

export type CreateDocumentMutationVariables = {
  input: CreateDocumentInput,
};

export type CreateDocumentMutation = {
  createDocument:  {
    __typename: "Document",
    id: string,
    documentName: string,
    dateTime: string,
    size: number | null,
    userEmail: string,
    owner: string | null,
  } | null,
};

export type UpdateDocumentMutationVariables = {
  input: UpdateDocumentInput,
};

export type UpdateDocumentMutation = {
  updateDocument:  {
    __typename: "Document",
    id: string,
    documentName: string,
    dateTime: string,
    size: number | null,
    userEmail: string,
    owner: string | null,
  } | null,
};

export type DeleteDocumentMutationVariables = {
  input: DeleteDocumentInput,
};

export type DeleteDocumentMutation = {
  deleteDocument:  {
    __typename: "Document",
    id: string,
    documentName: string,
    dateTime: string,
    size: number | null,
    userEmail: string,
    owner: string | null,
  } | null,
};

export type GetDocumentQueryVariables = {
  id: string,
};

export type GetDocumentQuery = {
  getDocument:  {
    __typename: "Document",
    id: string,
    documentName: string,
    dateTime: string,
    size: number | null,
    userEmail: string,
    owner: string | null,
  } | null,
};

export type ListDocumentsQueryVariables = {
  filter?: ModelDocumentFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListDocumentsQuery = {
  listDocuments:  {
    __typename: "ModelDocumentConnection",
    items:  Array< {
      __typename: "Document",
      id: string,
      documentName: string,
      dateTime: string,
      size: number | null,
      userEmail: string,
      owner: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateDocumentSubscriptionVariables = {
  owner: string,
};

export type OnCreateDocumentSubscription = {
  onCreateDocument:  {
    __typename: "Document",
    id: string,
    documentName: string,
    dateTime: string,
    size: number | null,
    userEmail: string,
    owner: string | null,
  } | null,
};

export type OnUpdateDocumentSubscriptionVariables = {
  owner: string,
};

export type OnUpdateDocumentSubscription = {
  onUpdateDocument:  {
    __typename: "Document",
    id: string,
    documentName: string,
    dateTime: string,
    size: number | null,
    userEmail: string,
    owner: string | null,
  } | null,
};

export type OnDeleteDocumentSubscriptionVariables = {
  owner: string,
};

export type OnDeleteDocumentSubscription = {
  onDeleteDocument:  {
    __typename: "Document",
    id: string,
    documentName: string,
    dateTime: string,
    size: number | null,
    userEmail: string,
    owner: string | null,
  } | null,
};
