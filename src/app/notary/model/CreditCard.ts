type ICardDataStatus = 'incomplete' | 'valid' | 'invalid';

interface ICardData {
  status: {
    cvc: ICardDataStatus;
    expiry: ICardDataStatus;
    name: ICardDataStatus;
    number: ICardDataStatus;
  };
  valid: boolean;
  values: {
    cvc: string;
    expiry: string;
    name: string;
    number: string;
    type?: string;
  };
}
