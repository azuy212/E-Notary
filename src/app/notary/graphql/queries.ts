// tslint:disable
// this is an auto generated file. This will be overwritten

export const getDocument = `query GetDocument($id: ID!) {
  getDocument(id: $id) {
    id
    documentName
    dateTime
    size
    userEmail
    owner
  }
}
`;
export const listDocuments = `query ListDocuments(
  $filter: ModelDocumentFilterInput
  $limit: Int
  $nextToken: String
) {
  listDocuments(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      documentName
      dateTime
      size
      userEmail
      owner
    }
    nextToken
  }
}
`;
