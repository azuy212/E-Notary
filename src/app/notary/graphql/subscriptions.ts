// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateDocument = `subscription OnCreateDocument($owner: String!) {
  onCreateDocument(owner: $owner) {
    id
    documentName
    dateTime
    size
    userEmail
    owner
  }
}
`;
export const onUpdateDocument = `subscription OnUpdateDocument($owner: String!) {
  onUpdateDocument(owner: $owner) {
    id
    documentName
    dateTime
    size
    userEmail
    owner
  }
}
`;
export const onDeleteDocument = `subscription OnDeleteDocument($owner: String!) {
  onDeleteDocument(owner: $owner) {
    id
    documentName
    dateTime
    size
    userEmail
    owner
  }
}
`;
