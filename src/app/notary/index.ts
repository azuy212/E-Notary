import { createSwitchNavigator } from 'react-navigation';

import NotaryMainAppNavigator from './screens/Main';
import NotaryAuthLoading from './screens/AuthLoading';
import NotaryAuthNavigator from './screens/UserManagement';

const NotaryNavigator = createSwitchNavigator({
  NotaryAuthLoading,
  NotaryAuth: NotaryAuthNavigator,
  NotaryMain: NotaryMainAppNavigator,
});

export default NotaryNavigator;
