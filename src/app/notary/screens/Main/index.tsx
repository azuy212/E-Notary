import React from 'react';
import { createStackNavigator, NavigationScreenProps } from 'react-navigation';
import MyScans from './MyScans';
import NotarizeScan from './NotarizeScan';
import { NotarizeContextProvider } from './NotarizeScan/NotarizeContext';

const NotaryMainAppNavigator = createStackNavigator(
  {
    Home: { screen: MyScans },
    NotarizeScan: {
      screen: (props: NavigationScreenProps) => (
        <NotarizeContextProvider>
          <NotarizeScan {...props} />
        </NotarizeContextProvider>
      ),
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
  },
);

export default NotaryMainAppNavigator;
