import React, { Component } from 'react';
import { KeyboardAvoidingView, ScrollView, StyleSheet } from 'react-native';
import { Card, CardItem, Text, H3 } from 'native-base';

import AddPaymentScreen from './AddPaymentScreen';
import { NotarizeContext } from '../NotarizeContext';

export default class Payment extends Component {
  static contextType = NotarizeContext;
  context!: React.ContextType<typeof NotarizeContext>;

  render() {
    const { length } = this.context.documentScans;
    const amount = 50 + length * 10;
    return (
      <KeyboardAvoidingView
        behavior='padding'
        style={styles.container}
        enabled={true}
        keyboardVerticalOffset={160}
      >
        <Card>
          <CardItem header={true} style={{ justifyContent: 'space-between' }}>
            <Text note={true}>Amount Payable (Pages: {length})</Text>
            <H3 style={styles.amount}>${(amount / 100).toFixed(2)}</H3>
          </CardItem>
          <ScrollView style={styles.container}>
            <CardItem header={true}>
              <Text>Payment Info</Text>
            </CardItem>
            <AddPaymentScreen />
          </ScrollView>
        </Card>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  amount: {
    fontWeight: 'bold',
  },
});
