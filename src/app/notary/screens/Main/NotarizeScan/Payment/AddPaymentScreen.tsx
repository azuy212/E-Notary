import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { CreditCardInput } from 'react-native-credit-card-input';
import { FontAwesome } from '@expo/vector-icons';

import { NotarizeContext } from '../NotarizeContext';
import PaymentReceiptModal from './PaymentReceiptModal';
import { getCreditCardToken, makePayment, makePaymentAlert } from '@/app/notary/services/payment';
import ButtonComponent from '@/components/ButtonComponent';

const STRIPE_ERROR = 'Payment service error. Try again later.';

interface IState {
  loading: boolean;
  submitted: boolean;
  modalOpen: boolean;
  error: string | null;
}

export default class AddPaymentScreen extends React.Component<{}, IState> {
  state = {
    loading: false,
    submitted: false,
    modalOpen: false,
    error: null,
  } as IState;

  static contextType = NotarizeContext;
  context!: React.ContextType<typeof NotarizeContext>;

  componentDidMount() {
    if (this.context.paymentReceiptURL) {
      this.setState({ submitted: true });
    }
  }

  setModalOpen = (modalOpen: boolean) => {
    this.setState({ modalOpen });
  }

  onSubmit = async (creditCardInput: ICardData) => {
    const amount = 50 + this.context.documentScans.length * 10;
    makePaymentAlert(
      amount,
      async () => {
        this.setState({ loading: true, error: null });
        try {
          const token = await getCreditCardToken(creditCardInput);
          const paymentResponse = await makePayment(token, amount);
          if (paymentResponse.status === 'succeeded') {
            this.setState({
              loading: false,
              submitted: true,
            });
            this.context.setPaymentReceiptURL(paymentResponse.receipt_url);
          }
        } catch (e) {
          console.log('Error', e);
          this.setState({
            submitted: false,
            loading: false,
            error: `${STRIPE_ERROR} Error Message: ${e.message}`,
          });
          return;
        }
      },
    );
  }

  render() {
    const { submitted, error, modalOpen, loading } = this.state;
    const { cardData, setCardData, setStepperPosition, stepperCurrentPosition } = this.context;
    return (
      <>
        <PaymentReceiptModal
          uri={this.context.paymentReceiptURL}
          modalOpen={modalOpen}
          setModalOpen={this.setModalOpen}
          onAccept={() => {
            this.setModalOpen(false);
            setStepperPosition(stepperCurrentPosition + 1);
          }}
        />
        <View style={styles.container}>
          <View style={styles.cardFormWrapper}>
              <CreditCardInput
                additionalInputsProps={{
                  number: { editable: !submitted },
                  cvc: { editable: !submitted },
                  expiry: { editable: !submitted },
                  name: { editable: !submitted },
                }}
                allowScroll={true}
                requiresName={true}
                onChange={cardData => setCardData(cardData)}
              />
            <View style={styles.buttonWrapper}>
              {submitted && (
                <View
                  style={[styles.alertWrapper, { backgroundColor: 'seagreen', marginBottom: 10 }]}
                >
                  <View style={styles.alertIconWrapper}>
                    <FontAwesome name='check' size={20} style={{ color: 'lightgreen' }} />
                  </View>
                  <View style={styles.alertTextWrapper}>
                    <Text style={[styles.alertText, { color: 'white' }]}>Payment Successful!</Text>
                  </View>
                </View>
              )}
              <ButtonComponent
                text={!submitted ? 'Make Payment' : 'Show Receipt'}
                loading={loading}
                onPress={() =>
                  cardData.valid && !loading
                    ? !submitted
                      ? this.onSubmit(cardData)
                      : this.setState({ modalOpen: true })
                    : null
                }
                buttonStyle={{
                  borderRadius: 5,
                  backgroundColor: cardData.valid ? 'black' : 'gray',
                  marginBottom: 0,
                }}
                buttonTextStyle={{ fontSize: 15, fontWeight: 'bold' }}
              />
              {/* Show errors */}
              {error && (
                <View style={styles.alertWrapper}>
                  <View style={styles.alertIconWrapper}>
                    <FontAwesome name='exclamation-circle' size={20} style={{ color: '#c22' }} />
                  </View>
                  <View style={styles.alertTextWrapper}>
                    <Text style={styles.alertText}>{error}</Text>
                  </View>
                </View>
              )}
            </View>
          </View>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textWrapper: {
    margin: 10,
  },
  infoText: {
    fontSize: 18,
    textAlign: 'center',
  },
  cardFormWrapper: {
    padding: 10,
    margin: 10,
  },
  buttonWrapper: {
    padding: 10,
    zIndex: 100,
  },
  alertTextWrapper: {
    flex: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertIconWrapper: {
    padding: 5,
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertText: {
    color: '#c22',
    fontSize: 16,
    fontWeight: '400',
  },
  alertWrapper: {
    backgroundColor: '#ecb7b7',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 5,
    paddingVertical: 5,
    marginTop: 10,
  },
});
