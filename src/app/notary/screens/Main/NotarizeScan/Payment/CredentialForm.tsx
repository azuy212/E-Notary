import React from 'react';
import { Form, Item, Label, Input, Grid, Col, Picker } from 'native-base';

interface IProps {
  paymentMethod: string;
  setPaymentMethod: React.Dispatch<React.SetStateAction<string>>;
}

const CredentialForm = ({ paymentMethod, setPaymentMethod }: IProps) => {
  return (
    <Form>
      <Item picker={true}>
        <Picker
          note={true}
          selectedValue={paymentMethod}
          onValueChange={item => setPaymentMethod(item)}
        >
          <Picker.Item label='Visa Card' value='visa' />
          <Picker.Item label='Master Card' value='master' />
        </Picker>
      </Item>
      <Item stackedLabel={true}>
        <Label>Cardholder Name</Label>
        <Input />
      </Item>
      <Item stackedLabel={true}>
        <Label>Card Number</Label>
        <Input placeholder='XXXX-XXXX-XXXX-XXXX' placeholderTextColor='lightgray' />
      </Item>
      <Item>
        <Grid>
          <Col>
            <Item stackedLabel={true}>
              <Label>CCV</Label>
              <Input placeholder='XXX' placeholderTextColor='lightgray' />
            </Item>
          </Col>
          <Col>
            <Item stackedLabel={true} last={true}>
              <Label>Expiration Date</Label>
              <Input placeholder='MM/YY' placeholderTextColor='lightgray' />
            </Item>
          </Col>
        </Grid>
      </Item>
    </Form>
  );
};

export default CredentialForm;
