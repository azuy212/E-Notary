import React, { useState } from 'react';
import { StyleSheet, Keyboard, Alert, Platform } from 'react-native';
import { NotarizeContextConsumer } from '../NotarizeContext';
import { Card, CardItem, Text, Item, Label, Input } from 'native-base';
import IconButton from '../components/IconButton';

import * as FileSystem from 'expo-file-system';
import { bytesConverter } from '@/services/common';
import { pushScanToCloud } from '@/app/notary/services/scans';
import ProgressModal from '@/components/ProgressModal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const Upload = () => {
  const [documentName, setDocumentName] = useState('');
  const [uploading, setUploading] = useState(false);
  const [size, setSize] = useState(-1);
  const [bytesUploaded, setBytesUploaded] = useState(0);

  return (
    <NotarizeContextConsumer>
      {({
        pdfDocumentURI,
        documentScans,
        documentUploaded,
        setDocumentUploaded,
        userEmail,
        setUserEmail,
      }) => {
        if (pdfDocumentURI) {
          FileSystem.getInfoAsync(pdfDocumentURI).then(res => setSize(res.size!));
        }
        const onFinishUpload = () => {
          setUploading(false);
          Alert.alert('Upload Finished', 'Document uploaded Successfully');
          setDocumentUploaded(true);
        };

        const uploadDocument = (uri: string) => {
          Keyboard.dismiss();
          if (documentName) {
            setUploading(true);
            pushScanToCloud(documentName, uri, userEmail, (progress) => {
              setBytesUploaded(progress.loaded);
              if (progress.loaded === progress.total) {
                onFinishUpload();
              }
            });
          } else {
            Alert.alert('Label not found!', 'Please provide a label for this document');
          }
        };
        return (
          <KeyboardAwareScrollView
            enableAutomaticScroll={true}
            extraScrollHeight={50}
            enableOnAndroid={true}
            extraHeight={Platform.select({ android: 50 })}
            style={{ flexGrow: 1 }}
          >
            <Card>
              <ProgressModal uploading={uploading} loaded={bytesUploaded} total={size} />
              <CardItem header={true}>
                <Text>Document Ready to Upload!</Text>
              </CardItem>
              <CardItem style={{ justifyContent: 'space-around' }}>
                <Text>Pages: {documentScans.length}</Text>
                <Text>Size: {bytesConverter(size)}</Text>
              </CardItem>
              <CardItem>
                <Item floatingLabel={true}>
                  <Label>Document Label</Label>
                  <Input value={documentName} onChangeText={setDocumentName} />
                </Item>
              </CardItem>
              <CardItem>
                <Item floatingLabel={true}>
                  <Label>Document Owner Email</Label>
                  <Input
                    value={userEmail}
                    keyboardType='email-address'
                    onChangeText={setUserEmail}
                  />
                </Item>
              </CardItem>
              <CardItem footer={true} style={{ justifyContent: 'flex-end' }}>
                <IconButton
                  loading={uploading}
                  disabled={documentUploaded}
                  icon='md-cloud-upload'
                  text='Upload'
                  onPress={() => uploadDocument(pdfDocumentURI)}
                />
              </CardItem>
              {documentUploaded && (
                <CardItem style={{ justifyContent: 'center' }}>
                  <Text style={{ color: 'green', fontWeight: 'bold' }}>
                    Document Uploaded Successfully!
                  </Text>
                </CardItem>
              )}
            </Card>
          </KeyboardAwareScrollView>
        );
      }}
    </NotarizeContextConsumer>
  );
};

export default Upload;
