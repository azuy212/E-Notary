import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content, Card, CardItem, Body, Toast } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';

import Stepper from './components/Stepper';
import StepperAction from '@/components/StepperAction';
import { NotarizeContext } from './NotarizeContext';

// import Form from './Form';
import Scan from './Scan';
import Upload from './Upload';
import Finish from './Finish';
import Prepare from './Prepare';
import Payment from './Payment';

interface IProps extends NavigationScreenProps {}

export default class NotarizeScan extends Component<IProps> {
  static contextType = NotarizeContext;
  context!: React.ContextType<typeof NotarizeContext>;

  handleNextPress = () => {
    const { stepperCurrentPosition, stepperLabels, setStepperPosition, documentUploaded, userEmail } = this.context;
    if (stepperCurrentPosition < stepperLabels.length - 1) {
      setStepperPosition(stepperCurrentPosition + 1);
    } else if (!userEmail) {
      return Toast.show({ text: 'You must provide a valid document owner email', type: 'danger' });
    } else if (!documentUploaded) {
      return Toast.show({ text: 'You need to upload your document', type: 'warning' });
    } else if (documentUploaded) {
      this.props.navigation.goBack();
    }
  }

  handleBackPress = () => {
    const { stepperCurrentPosition, setStepperPosition } = this.context;
    if (stepperCurrentPosition > 0) {
      setStepperPosition(stepperCurrentPosition - 1);
    }
  }

  renderStepperBody() {
    const { stepperCurrentPosition } = this.context;
    switch (stepperCurrentPosition) {
      case 0:
        return <Scan />;
      case 1:
        return <Prepare />;
      case 2:
        return <Payment />;
      case 3:
        return <Upload />;
      case 4:
        return <Finish {...this.props} />;
      default:
        return null;
    }
  }

  render() {
    return (
      <Container>
        <Content contentContainerStyle={styles.container}>
          <Card style={styles.card}>
            <CardItem header={true} style={styles.cardHeader}>
              <Stepper />
            </CardItem>
            <CardItem cardBody={true} style={styles.cardBody}>
              <Body>{this.renderStepperBody()}</Body>
            </CardItem>
            <CardItem footer={true} style={styles.cardFooter}>
              <StepperAction text='Back' position='left' icon='arrow-back' handler={this.handleBackPress} />
              <StepperAction text='Accept' position='right' icon='arrow-forward' handler={this.handleNextPress} />
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    flex: 1,
    alignSelf: 'stretch',
  },
  cardHeader: {
    flex: 0.2,
  },
  cardBody: {
    flex: 0.7,
    marginHorizontal: 10,
  },
  cardFooter: {
    flex: 0.1,
    justifyContent: 'space-between',
  },
});
