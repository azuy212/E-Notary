import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Text } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';

const Finish = (props: NavigationScreenProps) => {
  return (
    <View style={styles.container}>
      <Button onPress={() => props.navigation.goBack()}>
        <Text>Go to Home</Text>
      </Button>
    </View>
  );
};

export default Finish;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
  },
});
