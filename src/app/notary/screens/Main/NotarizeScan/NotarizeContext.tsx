import React, { Component } from 'react';

import { US_STATE_KEYS } from '@/app/notary/model/states';
import { FormKeys } from '@/app/notary/model/notarizeForm';
import { ImageSourcePropType } from 'react-native';
import { Toast, Root } from 'native-base';

import Auth from '@aws-amplify/auth';

interface IDocumentScan {
  key: string;
  source: ImageSourcePropType;
}

type ContextTypes = { [key in FormKeys]: string } & {
  state: US_STATE_KEYS;
  stepperCurrentPosition: number;
  stepperLabels: string[];
  documentScans: IDocumentScan[];
  pdfDocumentURI: string;
  paymentReceiptURL: string;
  cardData: ICardData;
  documentUploaded: boolean;
  userEmail: string;
  setFormData: (key: FormKeys, value: string) => void;
  setCardData: (cardData: ICardData) => void;
  setUserEmail: (email: string) => void;
  setStepperPosition: (position: number) => void;
  pushToDocumentScans: (scan: IDocumentScan) => void;
  deleteFromDocumentScans: (scan: IDocumentScan) => void;
  setPdfDocumentURI: (uri: string) => void;
  setPaymentReceiptURL: (url: string) => void;
  setDocumentUploaded: (uploaded: boolean) => void;
};

const STEPPER_LABELS = ['Scan', 'Prepare', 'Payment', 'Upload'];
const cardData: ICardData = {
  status: {
    cvc: 'incomplete',
    expiry: 'incomplete',
    name: 'incomplete',
    number: 'incomplete',
  },
  valid: false,
  values: {
    cvc: '',
    expiry: '',
    name: '',
    number: '',
    type: undefined,
  },
};

export const NotarizeContext = React.createContext<ContextTypes>({
  cardData,
  notaryNumber: '',
  name: '',
  state: 'AL',
  expirationDate: '',
  stepperCurrentPosition: 0,
  stepperLabels: STEPPER_LABELS,
  documentScans: [],
  pdfDocumentURI: '',
  paymentReceiptURL: '',
  documentUploaded: false,
  userEmail: '',
  setFormData: () => {},
  setCardData: () => {},
  setUserEmail: () => {},
  setStepperPosition: () => {},
  pushToDocumentScans: () => {},
  deleteFromDocumentScans: () => {},
  setPdfDocumentURI: () => {},
  setPaymentReceiptURL: () => {},
  setDocumentUploaded: () => {},
});

export class NotarizeContextProvider extends Component<any, ContextTypes> {
  state = {
    cardData,
    notaryNumber: '',
    name: '',
    state: 'AL',
    expirationDate: '',
    stepperCurrentPosition: 0,
    documentScans: [] as IDocumentScan[],
    pdfDocumentURI: '',
    paymentReceiptURL: '',
    documentUploaded: false,
    userEmail: '',
  } as ContextTypes;

  async componentDidMount() {
    try {
      const user = await Auth.currentAuthenticatedUser();
      const userAttributes = await Auth.userAttributes(user);
      const attributesObj = userAttributes.reduce((acc, curr) => {
        acc[curr.getName()] = curr.getValue();
        return acc;
      },                                          {} as Record<string, any>);
      this.setState({
        notaryNumber: user.username,
        name: attributesObj['custom:notary_name'],
        state: attributesObj['custom:state'],
        expirationDate: attributesObj['custom:expiration_date'],
      });
    } catch (error) {
      console.log('error', error);
    }
  }

  setFormData = (key: FormKeys, value: string) => {
    this.setState({
      [key]: value,
    } as Pick<ContextTypes, FormKeys>);
  }

  setCardData = (cardData: ICardData) => {
    this.setState({
      cardData,
    });
  }

  setUserEmail = (userEmail: string) => {
    this.setState({ userEmail });
  }

  setStepperPosition = (position: number) => {
    if (position > this.state.stepperCurrentPosition) {
      switch (this.state.stepperCurrentPosition) {
        case 0:
          if (this.state.documentScans.length === 0) {
            return Toast.show({ text: 'You need to scan at least 1 document to proceed', type: 'danger' });
          }
          break;
        case 1:
          if (!this.state.pdfDocumentURI) {
            return Toast.show({ text: 'You need to notarize your document to proceed', type: 'danger' });
          }
          break;
        case 2:
          if (!this.state.paymentReceiptURL) {
            return Toast.show({ text: 'You need to make a payment to proceed', type: 'danger' });
          }
          break;
      }
    }
    this.setState({ stepperCurrentPosition: position });
  }

  setPdfDocumentURI = (uri: string) => {
    this.setState({ pdfDocumentURI: uri });
  }

  setPaymentReceiptURL = (url: string) => {
    this.setState({ paymentReceiptURL: url });
  }

  setDocumentUploaded = (uploaded: boolean) => {
    this.setState({ documentUploaded: uploaded });
  }

  pushToDocumentScans = (scan: IDocumentScan) => {
    const { documentScans } = this.state;
    this.setState({
      documentScans: documentScans.concat([{ key: Date.now().toString(), source: scan.source }]),
    });
  }

  deleteFromDocumentScans = (scan: IDocumentScan) => {
    const { documentScans } = this.state;
    this.setState({
      documentScans: documentScans.filter(s => s.key !== scan.key),
    });
  }

  render() {
    const { ...state } = this.state;
    return (
      <NotarizeContext.Provider
        value={{
          ...state,
          stepperLabels: STEPPER_LABELS,
          setFormData: this.setFormData,
          setCardData: this.setCardData,
          setStepperPosition: this.setStepperPosition,
          pushToDocumentScans: this.pushToDocumentScans,
          deleteFromDocumentScans: this.deleteFromDocumentScans,
          setPdfDocumentURI: this.setPdfDocumentURI,
          setPaymentReceiptURL: this.setPaymentReceiptURL,
          setDocumentUploaded: this.setDocumentUploaded,
          setUserEmail: this.setUserEmail,
        }}
      >
        <Root>{this.props.children}</Root>
      </NotarizeContext.Provider>
    );
  }
}

export const NotarizeContextConsumer = NotarizeContext.Consumer;
