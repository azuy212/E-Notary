import React, { useState } from 'react';
import { Text, Grid, Row, Col } from 'native-base';
import { StyleSheet } from 'react-native';
import * as Print from 'expo-print';

import { US_STATES } from '@/services/common';
import KeyValue from './KeyValue';
import { generateHTML } from './config';
import PreviewModal from './PreviewModal';
import { NotarizeContextConsumer } from '../NotarizeContext';
import ImagesList from '../components/ImagesList';
import IconButton from '../components/IconButton';

const Prepare = () => {
  const [loading, setLoading] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);

  return (
    <NotarizeContextConsumer>
      {({
        notaryNumber,
        name,
        state,
        expirationDate,
        documentScans,
        pdfDocumentURI,
        setPdfDocumentURI,
        stepperCurrentPosition,
        setStepperPosition,
      }) => (
        <Grid style={styles.grid}>
          <Row size={0.75}>
            <Col>
              <KeyValue title='Notary #' value={notaryNumber} />
            </Col>
            <Col>
              <KeyValue title='Name' value={name} />
            </Col>
          </Row>
          <Row size={0.75}>
            <Col>
              <KeyValue title='State' value={US_STATES[state]} />
            </Col>
            <Col>
              <KeyValue title='Expiration Date' value={expirationDate} />
            </Col>
          </Row>
          <Row size={0.5}>
            <Text style={styles.keyTitle}>Scans:</Text>
          </Row>
          <Row size={4} style={styles.imagesListContainer}>
            <ImagesList showDeleteIcon={false} />
          </Row>
          <Row size={0.75} style={styles.actionButtons}>
            <PreviewModal
              uri={pdfDocumentURI}
              modalOpen={modalOpen}
              setModalOpen={setModalOpen}
              onAccept={() => {
                setModalOpen(false);
                setStepperPosition(stepperCurrentPosition + 1);
              }}
            />
            <IconButton
              text='Notarize'
              icon='watermark'
              iconType='MaterialCommunityIcons'
              loading={loading}
              onPress={async () => {
                setLoading(true);
                const response = await Print.printToFileAsync({
                  html: generateHTML(
                    { name, state, expirationDate, notaryNumber },
                    documentScans,
                  ),
                });
                setLoading(false);
                setPdfDocumentURI(response.uri);
              }}
            />
            <IconButton
              disabled={!pdfDocumentURI}
              text='Preview'
              icon='file-pdf'
              iconType='MaterialCommunityIcons'
              onPress={() => {
                setModalOpen(true);
              }}
            />
          </Row>
        </Grid>
      )}
    </NotarizeContextConsumer>
  );
};

export default Prepare;

const styles = StyleSheet.create({
  grid: {
    width: '100%',
  },
  keyTitle: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  imagesListContainer: {
    marginBottom: 10,
  },
  actionButtons: {
    justifyContent: 'space-between',
  },
});
