import React, { useState } from 'react';
import { View, Modal } from 'react-native';
import PDFReader from 'rn-pdf-reader-js';
import StepperAction from '@/components/StepperAction';
import { Text } from 'native-base';

interface IProps {
  uri: string;
  modalOpen: boolean;
  setModalOpen: (modal: boolean) => void;
  onAccept: () => void;
}

const PreviewModal = ({ uri, modalOpen, setModalOpen, onAccept }: IProps) => {
  const [loaded, setLoaded] = useState(false);
  return (
    <Modal
      animationType='slide'
      transparent={false}
      visible={modalOpen}
      onRequestClose={() => setModalOpen(false)}
    >
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.9 }}>
          <PDFReader
            source={{ uri }}
            customStyle={{
              readerContainer: { backgroundColor: 'white' },
              readerContainerNavigate: { backgroundColor: 'white' },
              readerContainerNavigateArrow: { color: 'black' },
            }}
            onLoad={() => setLoaded(true)}
          />
        </View>
        {loaded ? (
          <View style={{ flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' }}>
            <StepperAction
              text='Back'
              position='left'
              icon='arrow-back'
              handler={() => setModalOpen(false)}
            />
            <StepperAction text='Accept' position='right' icon='arrow-forward' handler={onAccept} />
          </View>
        ) : <Text style={{ textAlign: 'center' }}>Loading Preview...</Text>}
      </View>
    </Modal>
  );
};

export default PreviewModal;
