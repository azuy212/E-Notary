import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

interface IProps {
  title: string;
  value: string | number;
}

const KeyValue = (props: IProps) => {
  return (
    <View style={styles.field}>
      <Text style={styles.title}>{props.title}: </Text>
      <Text style={styles.value}>{props.value}</Text>
    </View>
  );
};

export default KeyValue;

const styles = StyleSheet.create({
  field: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  value: {
    fontSize: 16,
  },
});
