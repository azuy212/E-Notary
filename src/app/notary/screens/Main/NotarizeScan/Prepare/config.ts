import { FormKeys } from '@/app/notary/model/notarizeForm';
import { ImageSourcePropType, ImageURISource } from 'react-native';
import { US_STATES } from '@/services/common';
import { US_STATE_KEYS } from '@/app/notary/model/states';

const styles = `
<style>
  body, html {
    margin: 0px;
    height: 100%;
  }

  .page {
    margin: 0px;
    width: 100%;
    height: 98%;
    overflow: hidden;
    display: flex;
    justify-content: flex-end;
    align-items: flex-start;
    flex-direction: row;
  }

  .stamp {
    text-align: center;
    color: #555;
    font-size: 1.5rem;
    font-weight: 700;
    opacity: 0.7;
    margin: 10px;
    border: 0.25rem solid #555;
    display: inline-block;
    padding: 0.25rem 1rem;
    text-transform: uppercase;
    border-radius: 1rem;
    font-family: 'Courier';
    -webkit-mask-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/8399/grunge.png');
    -webkit-mask-size: 944px 604px;
    mix-blend-mode: multiply;
  }
</style>
`;

export interface IDocumentScan {
  key: string;
  source: ImageSourcePropType;
}

export function generateHTML(
  {
    notaryNumber,
    name,
    state,
    expirationDate,
  }: { [key in FormKeys]: string } & { state: US_STATE_KEYS },
  documentScans: IDocumentScan[],
) {
  return `
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="UTF-8" />
            ${styles}
            <title>Document Stamp</title>
          </head>
          <body>
          ${documentScans.map(
            document => `
          <div class="page" style="background: url('${
            (document.source as ImageURISource).uri
          }') scroll no-repeat center/cover;">
            <span class="stamp">
              <div>${name}</div>
              <div>Commission #${notaryNumber}</div>
              <div>Notary Public</div>
              <div>State of ${US_STATES[state]}</div>
              <div>My Commission Expires ${expirationDate}</div>
            </span>
          </div>`,
          )}
          </body>
        </html>
        `;
}
