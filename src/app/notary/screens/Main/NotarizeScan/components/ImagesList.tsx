import React from 'react';
import { FlatList, Image, StyleSheet } from 'react-native';
import { Text, View, Icon } from 'native-base';

import { IMAGE_SIZE, LIST_COLUMNS, VIEW_MARGIN } from '../Scan/config';
import { NotarizeContextConsumer } from '../NotarizeContext';

interface IProps {
  showDeleteIcon?: boolean;
}
const ImagesList = ({ showDeleteIcon }: IProps) => {
  // tslint:disable-next-line: no-boolean-literal-compare
  const showIcon = showDeleteIcon === false ? false : true;
  return (
    <NotarizeContextConsumer>
      {({ documentScans, deleteFromDocumentScans }) => (
        <FlatList
          numColumns={LIST_COLUMNS}
          data={documentScans}
          ListEmptyComponent={() => (
            <Text style={styles.noDataText} note={true}>
              {'No document scanned yet'}
            </Text>
          )}
          renderItem={({ item }) => (
            <View style={{ margin: VIEW_MARGIN }}>
              {showIcon ? (
                <Icon
                  onPress={() => deleteFromDocumentScans(item)}
                  style={styles.closeButton}
                  name='ios-close-circle'
                />
              ) : null}
              <Image
                source={item.source}
                style={{ resizeMode: 'contain' }}
                width={IMAGE_SIZE}
                height={IMAGE_SIZE}
              />
            </View>
          )}
        />
      )}
    </NotarizeContextConsumer>
  );
};

export default ImagesList;

const styles = StyleSheet.create({
  noDataText: {
    textAlign: 'center',
    alignSelf: 'center',
  },
  closeButton: {
    position: 'absolute',
    right: -5,
    top: -10,
    zIndex: 1,
  },
});
