import React from 'react';
import { Button, Text, Icon, Spinner } from 'native-base';

type IconTypes =
  | 'MaterialCommunityIcons'
  | 'AntDesign'
  | 'Entypo'
  | 'EvilIcons'
  | 'Feather'
  | 'FontAwesome'
  | 'FontAwesome5'
  | 'Foundation'
  | 'Ionicons'
  | 'MaterialIcons'
  | 'Octicons'
  | 'SimpleLineIcons'
  | 'Zocial'
  | undefined;

interface IProps {
  text: string;
  icon: string;
  iconType?: IconTypes;
  disabled?: boolean;
  loading?: boolean;
  onPress: () => void;
}

const IconButton = ({
  icon,
  iconType,
  loading,
  onPress,
  text,
  disabled,
}: IProps) => {
  return (
    <Button iconLeft={true} dark={true} onPress={onPress} disabled={disabled}>
      {loading ? (
        <Spinner style={{ marginLeft: 5 }} />
      ) : (
        <Icon type={iconType} name={icon} />
      )}
      <Text>{text}</Text>
    </Button>
  );
};

export default IconButton;
