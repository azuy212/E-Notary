import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { CreditCardInput } from 'react-native-credit-card-input';
import { FontAwesome } from '@expo/vector-icons';
import ButtonComponent from '@/components/ButtonComponent';

interface IProps {
  loading: boolean;
  submitted: boolean;
  error: string | null;
  onSubmit: (cardData: ICardData) => void;
}

interface IState {
  cardData: ICardData;
}

export default class AddSubscriptionView extends React.Component<IProps, IState> {
  state = {
    cardData: {
      status: {
        cvc: 'incomplete',
        expiry: 'incomplete',
        name: 'incomplete',
        number: 'incomplete',
      },
      valid: false,
      values: {
        cvc: '',
        expiry: '',
        name: '',
        number: '',
        type: undefined,
      },
    } as ICardData,
  };

  render() {
    const { onSubmit, submitted, error, loading } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.cardFormWrapper}>
          <View>
            <CreditCardInput
              requiresName={true}
              onChange={cardData => this.setState({ cardData })}
            />
          </View>
          <View style={styles.buttonWrapper}>
            <ButtonComponent
              text='Make Payment'
              onPress={() => onSubmit(this.state.cardData)}
              loading={loading}
            />
            {/* Show errors */}
            {error && (
              <View style={styles.alertWrapper}>
                <View style={styles.alertIconWrapper}>
                  <FontAwesome name='exclamation-circle' size={20} style={{ color: '#c22' }} />
                </View>
                <View style={styles.alertTextWrapper}>
                  <Text style={styles.alertText}>{error}</Text>
                </View>
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textWrapper: {
    margin: 10,
  },
  infoText: {
    fontSize: 18,
    textAlign: 'center',
  },
  cardFormWrapper: {
    padding: 10,
    margin: 10,
  },
  buttonWrapper: {
    padding: 10,
    zIndex: 100,
  },
  alertTextWrapper: {
    flex: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertIconWrapper: {
    padding: 5,
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertText: {
    color: '#c22',
    fontSize: 16,
    fontWeight: '400',
  },
  alertWrapper: {
    backgroundColor: '#ecb7b7',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 5,
    paddingVertical: 5,
    marginTop: 10,
  },
});
