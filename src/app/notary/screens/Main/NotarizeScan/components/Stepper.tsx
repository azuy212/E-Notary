import React from 'react';
import { View } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import { StepperStyle } from '@/app/notary/services/stepper';
import { NotarizeContextConsumer } from '../NotarizeContext';

const Stepper = () => {
  return (
    <NotarizeContextConsumer>
      {({ stepperCurrentPosition, stepperLabels, setStepperPosition }) => (
        <View style={{ flex: 1 }}>
          <StepIndicator
            direction='horizontal'
            currentPosition={stepperCurrentPosition}
            customStyles={StepperStyle}
            labels={stepperLabels}
            stepCount={stepperLabels.length}
            onPress={setStepperPosition}
          />
        </View>
      )}
    </NotarizeContextConsumer>
  );
};

export default Stepper;
