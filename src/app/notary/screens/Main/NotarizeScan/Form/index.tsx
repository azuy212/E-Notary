import React from 'react';
import { Form, Item, Label, Input } from 'native-base';

import { getDateString } from '@/services/common';

import StatePicker from '@/components/StatePicker';
import ExpirationDatePicker from '@/components/ExpirationDatePicker';
import { NotarizeContextConsumer } from '@/app/notary/screens/Main/NotarizeScan/NotarizeContext';

const NotarizeForm = () => {
  return (
    <NotarizeContextConsumer>
      {({ setFormData, notaryNumber, name, expirationDate, state }) => (
        <Form>
          <Item stackedLabel={true} error={!notaryNumber} success={!!notaryNumber}>
            <Label>Notary #</Label>
            <Input value={notaryNumber} onChangeText={text => setFormData('notaryNumber', text)} />
          </Item>
          <Item stackedLabel={true} error={!name} success={!!name}>
            <Label>Name</Label>
            <Input value={name} onChangeText={text => setFormData('name', text)} />
          </Item>
          <Item stackedLabel={true} error={!state} success={!!state}>
            <Label>State</Label>
            <StatePicker
              selectedState={state}
              onStateSelected={value => setFormData('state', value)}
            />
          </Item>
          <Item stackedLabel={true} error={!expirationDate} success={!!expirationDate}>
            <Label>Commission Expiration Date</Label>
            <ExpirationDatePicker
              selectedDate={expirationDate}
              onDateSelected={date =>
                setFormData('expirationDate', getDateString(date))
              }
            />
          </Item>
        </Form>
      )}
    </NotarizeContextConsumer>
  );
};

export default NotarizeForm;
