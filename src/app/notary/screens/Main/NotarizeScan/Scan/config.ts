import { Dimensions } from 'react-native';

const SCREEN_WIDTH = Dimensions.get('screen').width;
export const IMAGE_SIZE = SCREEN_WIDTH / 2.5;
export const VIEW_MARGIN = 10;

export const LIST_COLUMNS = Math.floor(
  SCREEN_WIDTH / (IMAGE_SIZE + 2 * VIEW_MARGIN),
);
