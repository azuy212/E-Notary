import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Fab, Icon } from 'native-base';
import * as ImagePicker from 'expo-image-picker';

import { askForCameraPermissions } from '@/services/common';
import ImagesList from '../components/ImagesList';
import { NotarizeContext } from '../NotarizeContext';

export default class Scan extends Component {
  static contextType = NotarizeContext;
  context!: React.ContextType<typeof NotarizeContext>;

  launchCamera = async () => {
    try {
      const permissionsGranted = await askForCameraPermissions();
      if (permissionsGranted) {
        const imageResult = await ImagePicker.launchCameraAsync({
          allowsEditing: true,
          quality: 1,
        });
        if (!imageResult.cancelled) {
          this.context.pushToDocumentScans({
            key: '',
            source: { uri: imageResult.uri },
          });
        }
      }
    } catch (error) {
      console.log('Error:', error);
    }
  }

  render() {
    return (
      <View style={{ flex: 1, alignSelf: 'stretch' }}>
        <ImagesList />
        <Fab style={styles.fab} onPress={this.launchCamera}>
          <Icon type='MaterialIcons' name='add-a-photo' />
        </Fab>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fab: {
    backgroundColor: 'black',
  },
});
