import { ActionSheet } from 'native-base';
import { Alert, SectionListData } from 'react-native';
import { IRawScanData } from '@/model/scan';
import { bytesConverter } from '@/services/common';

type IASButtons = {
  text: string;
  icon?: string;
  iconColor?: string;
};

const ACTION_SHEET_BUTTONS: IASButtons[] = [
  { text: 'Details', icon: 'ios-information-circle-outline' },
  { text: 'Delete', icon: 'trash', iconColor: '#fa213b' },
  { text: 'Cancel', icon: 'close', iconColor: '#25de5b' },
];

const ScanListActionSheet = (
  scan: IRawScanData,
  section: SectionListData<any>,
  deleteScan: (item: IRawScanData) => void,
) => {
  ActionSheet.show(
    {
      options: ACTION_SHEET_BUTTONS,
      destructiveButtonIndex: 1,
      cancelButtonIndex: 2,
    },
    (index) => {
      const title = ACTION_SHEET_BUTTONS[index].text;
      switch (index) {
        case 0:
          Alert.alert(
            title,
            `
Label: ${scan.key}
Size: ${bytesConverter(scan.size!)}
Last Modified: ${section.date} ${scan.lastModified}
          `,
          );
          break;
        case 1:
          Alert.alert(
            `Delete '${scan.key}'`,
            'Are you sure to delete this scan',
            [
              { text: 'Cancel', style: 'cancel' },
              {
                text: 'Delete',
                style: 'destructive',
                onPress: () => deleteScan(scan),
              },
            ],
          );

          break;
        default:
          break;
      }
    },
  );
};

export default ScanListActionSheet;
