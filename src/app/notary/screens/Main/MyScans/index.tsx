import React, { Component } from 'react';
import { Alert, StyleSheet } from 'react-native';
import { Container, Fab, Icon, Content, Root, Toast } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';

import { getScansByDate } from '@/services/common';
import { IRawScanData } from '@/model/scan';
import { getScansFromCloud, deleteScanFromCloud } from '@/app/notary/services/scans';
import MyHeader from '@/components/MyHeader';

import ScansList from './ScansList';
import { showErrorAlert } from '@/services/error';

interface IState {
  ready: boolean;
  scans: IRawScanData[];
  filteredScans: IRawScanData[];
  refreshing: boolean;
}

type IProps = NavigationScreenProps;

export default class MyScans extends Component<IProps, IState> {
  state = {
    ready: false,
    scans: [],
    filteredScans: [],
    refreshing: false,
  } as IState;

  async componentDidMount() {
    this.props.navigation.addListener('didFocus', () => {
      this.setState({ refreshing: true });
      this.fetchScans();
    });
  }

  fetchScans = async () => {
    try {
      const scans = await getScansFromCloud();
      this.setState({
        scans,
        filteredScans: scans,
        refreshing: false,
        ready: true,
      });
    } catch (error) {
      console.log('Error Fetching', error);
      showErrorAlert('Error Fetching Scans', error);
    }
  }

  deleteScan = async (item: IRawScanData) => {
    this.setState({ refreshing: true });
    Toast.show({
      text: `Deleting scan "${item.key}"...`,
      type: 'danger',
      duration: 3000,
    });
    await deleteScanFromCloud(item);
    this.fetchScans();
  }

  handleSearchTextChange = (text: string) => {
    const filteredScans = this.state.scans.filter(value =>
      value.key.toLowerCase().includes(text.toLowerCase()),
    );
    this.setState({ filteredScans });
  }

  handleListRefresh = () => {
    this.setState({ refreshing: true });
    this.fetchScans();
  }

  handleFabIconPress = () => {
    this.props.navigation.navigate('NotarizeScan');
  }

  render() {
    const { ready, refreshing, filteredScans } = this.state;
    return (
      <Root>
        <Container>
          <MyHeader
            onSearchTextChange={this.handleSearchTextChange}
            navigateTo='NotaryAuthLoading'
            {...this.props}
          />
          <Content contentContainerStyle={styles.listContainer}>
            <ScansList
              listRefreshHandler={this.handleListRefresh}
              deleteScan={this.deleteScan}
              scans={getScansByDate(filteredScans)}
              ready={ready}
              refreshing={refreshing}
            />
          </Content>
          <Fab style={styles.fab} onPress={this.handleFabIconPress}>
            <Icon style={styles.fabIcon} type='MaterialIcons' name='note-add' />
          </Fab>
        </Container>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    flex: 1,
  },
  fab: {
    backgroundColor: 'black',
  },
  fabIcon: {
    fontSize: 30,
  },
});
