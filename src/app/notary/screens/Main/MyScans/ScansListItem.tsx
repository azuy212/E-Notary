import React from 'react';
import { ListItem, Left, Thumbnail, Body, Text } from 'native-base';

import { IRawScanData } from '@/model/scan';
import ScanListActionSheet from './ActionSheet';
import { SectionListData } from 'react-native';

interface IProps {
  item: IRawScanData;
  section: SectionListData<any>;
  deleteScan: (item: IRawScanData) => void;
}

const ScansListItem = ({ item, section, deleteScan }: IProps) => {
  return (
    <ListItem thumbnail={true} onPress={() => ScanListActionSheet(item, section, deleteScan)}>
      <Left>
        <Thumbnail square={true} source={require('../../../../../../assets/images/pdf.png')} />
      </Left>
      <Body>
        <Text>{item.key}</Text>
        <Text note={true} numberOfLines={1}>
          {item.lastModified}
        </Text>
      </Body>
    </ListItem>
  );
};

export default ScansListItem;
