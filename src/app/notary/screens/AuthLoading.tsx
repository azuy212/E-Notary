import React from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import Auth from '@aws-amplify/auth';
import * as Font from 'expo-font';

interface State {
  userToken: any;
}

export default class NotaryAuthLoading extends React.Component<NavigationScreenProps, State> {
  state = {
    userToken: null,
  };

  async componentDidMount() {
    await this.loadApp();
  }

  // Get the logged in users and remember them
  loadApp = async () => {
    await Auth.currentAuthenticatedUser()
      .then((user: any) => {
        this.setState({
          userToken: user.signInUserSession.accessToken.jwtToken,
        });
      })
      .catch(err => console.log(err));

    await Font.loadAsync({
      Roboto: require('../../../../node_modules/native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('../../../../node_modules/native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('../../../../node_modules/native-base/Fonts/Ionicons.ttf'),
    });

    this.props.navigation.navigate(this.state.userToken ? 'NotaryMain' : 'NotaryAuth');
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size='large' color='#fff' />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
