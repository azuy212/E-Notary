import { createStackNavigator } from 'react-navigation';

import NotarySignInScreen from './SignInScreen';
import NotarySignUpScreen from './SignUpScreen';
import NotaryForgetPasswordScreen from './ForgetPasswordScreen';

const NotaryAuthNavigator = createStackNavigator({
  NotarySignIn: {
    screen: NotarySignInScreen,
    navigationOptions: () => ({
      title: 'Log in to Notary Account',
    }),
  },
  NotarySignUp: {
    screen: NotarySignUpScreen,
    navigationOptions: () => ({
      title: 'New Notary Account',
    }),
  },
  NotaryForgetPassword: {
    screen: NotaryForgetPasswordScreen,
    navigationOptions: () => ({
      title: 'Forgot your Password?',
    }),
  },
});

export default NotaryAuthNavigator;
