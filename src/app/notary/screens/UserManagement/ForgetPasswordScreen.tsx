import React from 'react';
import { StyleSheet, View, Alert, Image, Platform } from 'react-native';

import Auth from '@aws-amplify/auth';

import { Item, Input, Icon, Form } from 'native-base';

import Button from '@/components/ButtonComponent';
import { NavigationScreenProps } from 'react-navigation';

// Load the app logo
import logo from '../../../../../assets/images/logo.png';
import { STYLES } from '@/themes';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { showErrorAlert } from '@/services/error';

interface State {
  username: string;
  authCode: string;
  newPassword: string;
  loading1: boolean;
  loading2: boolean;
}

type StateKeys = keyof State;

export default class ForgetPasswordScreen extends React.Component<NavigationScreenProps, State> {
  state = {
    username: '',
    authCode: '',
    newPassword: '',
    loading1: false,
    loading2: false,
  };

  onChangeText(key: StateKeys, value: any) {
    this.setState({
      [key]: value,
    } as Pick<State, StateKeys>);
  }
  // Request a new password
  forgotPassword = async () => {
    const { username } = this.state;
    this.setState({ loading1: true });
    await Auth.forgotPassword(username)
      .then(() => {
        this.setState({ loading1: false });
        console.log('Verification Code has been sent');
        Alert.alert(
          'Verification Code Sent',
          'Please check your associated email and provide new password with verification code',
        );
      })
      .catch((err) => {
        this.setState({ loading1: false });
        showErrorAlert('Error while setting up the new password: ', err);
      });
  }
  // Upon confirmation redirect the user to the Sign In page
  forgotPasswordSubmit = async () => {
    const { username, authCode, newPassword } = this.state;
    this.setState({ loading2: true });
    await Auth.forgotPasswordSubmit(username, authCode, newPassword)
      .then(() => {
        this.setState({ loading2: false });
        Alert.alert('Password Changed Successfully', 'Your password has been updated ');
        this.props.navigation.navigate('SignIn');
      })
      .catch((err) => {
        this.setState({ loading2: false });
        showErrorAlert('Error while confirming the new password: ', err);
      });
  }

  render() {
    return (
      <KeyboardAwareScrollView
        enableAutomaticScroll={true}
        extraScrollHeight={100}
        enableOnAndroid={true}
        extraHeight={Platform.select({ android: 100 })}
        style={{ flexGrow: 1 }}
      >
        <Image source={logo} style={[STYLES.LOGO.IMAGE, { alignSelf: 'center' }]} />
        <Form style={{ flex: 1, paddingHorizontal: 20 }}>
          {/* Username */}
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='hash' type='Feather' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Notary Number'
              placeholderTextColor='#adb4bc'
              keyboardType={'email-address'}
              returnKeyType='go'
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={value => this.onChangeText('username', value)}
            />
          </Item>
          <Button
            onPress={this.forgotPassword}
            loading={this.state.loading1}
            text='Send Code'
            buttonStyle={STYLES.BUTTON.AUTH_SCREEN.BUTTON}
            buttonTextStyle={STYLES.BUTTON.AUTH_SCREEN.TEXT}
          />
          <View style={{ marginTop: 15 }}>
            <Item rounded={true} style={styles.itemStyle}>
              <Icon active={true} name='lock' style={STYLES.INPUT.ICON} />
              <Input
                style={STYLES.INPUT.TEXT}
                placeholder='New password'
                placeholderTextColor='#adb4bc'
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                secureTextEntry={true}
                onChangeText={value => this.onChangeText('newPassword', value)}
              />
            </Item>

            {/**************** Code confirmation section  *****************/}

            <Item rounded={true} style={styles.itemStyle}>
              <Icon active={true} name='md-apps' style={STYLES.INPUT.ICON} />
              <Input
                style={STYLES.INPUT.TEXT}
                placeholder='Confirmation code'
                placeholderTextColor='#adb4bc'
                keyboardType={'numeric'}
                returnKeyType='done'
                autoCapitalize='none'
                autoCorrect={false}
                secureTextEntry={false}
                onChangeText={value => this.onChangeText('authCode', value)}
              />
            </Item>
            <Button
              onPress={this.forgotPasswordSubmit}
              loading={this.state.loading2}
              text='Confirm the new password'
              buttonStyle={STYLES.BUTTON.AUTH_SCREEN.BUTTON}
              buttonTextStyle={STYLES.BUTTON.AUTH_SCREEN.TEXT}
            />
          </View>
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  infoContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    height: 'auto',
    bottom: 25,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    backgroundColor: '#ffffff',
  },
  itemStyle: {
    marginBottom: 20,
  },
});
