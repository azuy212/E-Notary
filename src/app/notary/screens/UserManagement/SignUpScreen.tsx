import React from 'react';
import { StyleSheet, Alert, Image, Dimensions, Platform } from 'react-native';

import { Item, Input, Icon, Form, Label } from 'native-base';

// AWS Amplify
import Auth from '@aws-amplify/auth';

// Import data for countries
import data from '@/app/notary/services/countriesData';

import Button from '@/components/ButtonComponent';

import Dialog from 'react-native-dialog';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// Load the app logo
import logo from '../../../../../assets/images/logo.png';
import { NavigationScreenProps } from 'react-navigation';
import { STYLES } from '@/themes';
import { handleConfirmSignUpDialog } from '@/services/userManagement';
import StatePicker from '@/components/StatePicker';
import { US_STATE_KEYS } from '@/app/notary/model/states';
import { getDateString } from '@/services/common';
import ExpirationDatePicker from '@/components/ExpirationDatePicker';
import { showErrorAlert } from '@/services/error';
import PasswordValidation from '@/services/password';

// Default render of country flag
const defaultFlag = data.filter(obj => obj.name === 'United States')[0].flag;

const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 2;

interface State {
  notaryName: string;
  username: string;
  password: string;
  validPassword: boolean | null;
  passwordMessage: string;
  email: string;
  state: US_STATE_KEYS;
  expirationDate: string;
  phoneNumber: string;
  flag: string;
  modalVisible: boolean;
  authCode: string;
  loading: boolean;
  confirmCodeDialog: boolean;
}

type StateKeys = keyof State;

export default class SignUpScreen extends React.Component<NavigationScreenProps, State> {
  state = {
    notaryName: '',
    username: '',
    password: '',
    validPassword: null,
    passwordMessage: '',
    email: '',
    state: 'AL',
    expirationDate: '',
    phoneNumber: '',
    flag: defaultFlag,
    modalVisible: false,
    authCode: '',
    loading: false,
    confirmCodeDialog: false,
  } as State;
  // Get user input
  onChangeText(key: StateKeys, value: any) {
    this.setState({ [key]: value } as Pick<State, StateKeys>, () => {
      if (key === 'password') {
        this.validatePassword();
      }
    });
  }
  // Functions for Phone Input
  showModal(modalVisible: boolean) {
    this.setState({ modalVisible });
  }

  async getCountry(country: string) {
    const countryData = data;
    try {
      const countryCode = countryData.filter(obj => obj.name === country)[0].dial_code;
      const countryFlag = countryData.filter(obj => obj.name === country)[0].flag;
      // Set data from user choice of country
      this.setState({ phoneNumber: countryCode, flag: countryFlag });
      this.showModal(false);
    } catch (err) {
      console.log(err);
    }
  }
  // Sign up user with AWS Amplify Auth
  async signUp() {
    const {
      notaryName,
      username,
      password,
      validPassword,
      passwordMessage,
      email,
      phoneNumber,
      state,
      expirationDate,
    } = this.state;
    // rename variable to conform with Amplify Auth field phone attribute
    this.setState({ loading: true });
    try {
      if (!username || !notaryName || !password || !email || !expirationDate) {
        throw new Error('Please fill all fields');
      } else if (!validPassword) {
        throw new Error(`Password should contain ${passwordMessage}`);
      } else {
        await Auth.signUp({
          username,
          password,
          attributes: {
            email,
            phone_number: phoneNumber,
            name: notaryName,
            'custom:notary_name': notaryName,
            'custom:state': state,
            'custom:expiration_date': expirationDate,
          },
        });
        this.setState({ loading: false, confirmCodeDialog: true });
      }
    } catch (error) {
      this.setState({ loading: false });
      const err = error.message ? error.message : error;
      showErrorAlert('Error when signing up: ', err);
    }
  }
  // Confirm users and redirect them to the SignIn page
  async confirmSignUp() {
    const { username, authCode } = this.state;
    await Auth.confirmSignUp(username, authCode)
      .then(() => {
        this.setState({ confirmCodeDialog: false });
        Alert.alert(
          'Account Verified!',
          `Congratulations! Your account has been verified successfully,
           Please Sign In to your account`,
          [
            {
              text: 'Sign In',
              onPress: () => this.props.navigation.navigate('SignIn'),
            },
          ],
        );
      })
      .catch(err => showErrorAlert('Error when entering confirmation code: ', err));
  }
  // Resend code if not received already
  async resendSignUp() {
    const { username } = this.state;
    await Auth.resendSignUp(username)
      .then(() => {
        Alert.alert('Confirmation code resent successfully');
      })
      .catch(err => showErrorAlert('Error requesting new confirmation code: ', err));
  }

  hideConfirmCodeDialog = () => {
    this.setState({ confirmCodeDialog: false });
  }

  validatePassword = () => {
    const validPassword = PasswordValidation.validate(this.state.password);
    const passwordMessage = PasswordValidation.getMessage(this.state.password)!;
    this.setState({ validPassword, passwordMessage });
  }

  render() {
    const { state, expirationDate, validPassword } = this.state;
    return (
      <KeyboardAwareScrollView
        enableAutomaticScroll={true}
        extraScrollHeight={100}
        enableOnAndroid={true}
        extraHeight={Platform.select({ android: 100 })}
        style={{ flexGrow: 1 }}
      >
        <Image source={logo} style={[STYLES.LOGO.IMAGE, { alignSelf: 'center' }]} />
        <Form style={{ flex: 1, paddingHorizontal: 20 }}>
          {/* Notary Name section  */}
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='ios-person' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Notary Name'
              placeholderTextColor='#adb4bc'
              onChangeText={value => this.onChangeText('notaryName', value)}
            />
          </Item>
          {/* Notary Number section  */}
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='hash' type='Feather' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Notary Number'
              placeholderTextColor='#adb4bc'
              onChangeText={value => this.onChangeText('username', value)}
            />
          </Item>
          {/*  password section  */}
          <Item
            rounded={true}
            style={styles.itemStyle}
            error={validPassword !== null && !validPassword}
            success={validPassword!}
          >
            <Icon active={true} name='lock' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Password'
              placeholderTextColor='#adb4bc'
              secureTextEntry={true}
              onChangeText={value => this.onChangeText('password', value)}
              onBlur={this.validatePassword}
            />
            <Label style={{ fontSize: 10 }}>{this.state.passwordMessage}</Label>
            {validPassword && <Icon name='checkmark-circle' />}
            {validPassword !== null && !validPassword && <Icon name='close-circle' />}
          </Item>
          {/* email section */}
          <Item rounded={true} style={styles.itemStyle}>
            <Icon name='mail' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Email'
              placeholderTextColor='#adb4bc'
              keyboardType={'email-address'}
              onChangeText={value => this.onChangeText('email', value)}
            />
          </Item>
          {/* State Picker */}
          <Item rounded={true} picker={true} style={styles.itemStyle}>
            <Icon name='location-city' type='MaterialIcons' style={STYLES.INPUT.ICON} />
            <StatePicker
              selectedState={state}
              onStateSelected={value => this.onChangeText('state', value)}
            />
          </Item>
          {/* Expiration Date Picker */}
          <Item rounded={true} style={[styles.itemStyle, { height: window.height / 15 }]}>
            <Icon name='date-range' type='MaterialIcons' style={STYLES.INPUT.ICON} />
            <ExpirationDatePicker
              placeHolder='Commission Expiration Date'
              selectedDate={expirationDate}
              onDateSelected={date => this.onChangeText('expirationDate', getDateString(date))}
            />
          </Item>
          <Button
            onPress={() => this.signUp()}
            loading={this.state.loading}
            text='Sign Up'
            buttonStyle={[STYLES.BUTTON.AUTH_SCREEN.BUTTON, { marginTop: 20 }]}
            buttonTextStyle={STYLES.BUTTON.AUTH_SCREEN.TEXT}
          />
          <Dialog.Container visible={this.state.confirmCodeDialog}>
            <Dialog.Title>Enter Confirmation Code</Dialog.Title>
            <Dialog.Description>
              Verification Code has been sent to the following email
            </Dialog.Description>
            <Dialog.Description style={{ fontWeight: 'bold' }}>
              {this.state.email}
            </Dialog.Description>
            <Dialog.Description>
              Please check your email and provide verification code
            </Dialog.Description>
            <Dialog.Input
              label='Verification Code'
              onChangeText={value => this.onChangeText('authCode', value)}
              style={{
                borderBottomColor: '#ccc',
                borderBottomWidth: 2,
              }}
            />
            <Dialog.Button label='Resend Code' onPress={() => this.resendSignUp()} />
            <Dialog.Button
              label='Cancel'
              onPress={() => handleConfirmSignUpDialog(this.hideConfirmCodeDialog)}
            />
            <Dialog.Button label='Verify' onPress={() => this.confirmSignUp()} />
          </Dialog.Container>
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemStyle: {
    marginBottom: 10,
  },
  iconStyle: {
    color: '#5a52a5',
    fontSize: 28,
    marginLeft: 15,
  },
  textStyle: {
    padding: 5,
    fontSize: 18,
  },
  countryStyle: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopColor: '#211f',
    borderTopWidth: 1,
    padding: 12,
  },
  closeButtonStyle: {
    flex: 1,
    padding: 12,
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#211f',
    backgroundColor: '#fff3',
  },
  buttonLink: {
    color: 'blue',
    textDecorationLine: 'underline',
  },
});
