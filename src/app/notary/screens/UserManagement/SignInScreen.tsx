import React from 'react';
import { StyleSheet, View, Image, Platform, Alert } from 'react-native';

import Auth from '@aws-amplify/auth';

import { Item, Input, Icon, Text, Form } from 'native-base';

import Button from '@/components/ButtonComponent';
import { NavigationScreenProps } from 'react-navigation';

// Load the app logo
import logo from '../../../../../assets/images/logo.png';
import { STYLES } from '@/themes';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { showErrorAlert } from '@/services/error';

interface State {
  username: string;
  password: string;
  loading: boolean;
}

type StateKeys = keyof State;

export default class SignInScreen extends React.Component<NavigationScreenProps, State> {
  static navigationOptions = { header: null };

  state = {
    username: '',
    password: '',
    loading: false,
  };

  onChangeText(key: StateKeys, value: any) {
    this.setState({
      [key]: value,
    } as Pick<State, StateKeys>);
  }

  async signIn() {
    const { username, password } = this.state;
    this.setState({ loading: true });
    await Auth.signIn(username, password)
      .then(() => {
        this.setState({ loading: false });
        this.props.navigation.navigate('NotaryAuthLoading');
      })
      .catch((err) => {
        this.setState({ loading: false });
        showErrorAlert('Error when signing in: ', err);
      });
  }

  render() {
    return (
      <KeyboardAwareScrollView
        enableAutomaticScroll={true}
        extraScrollHeight={100}
        enableOnAndroid={true}
        extraHeight={Platform.select({ android: 100 })}
        style={{ flexGrow: 1 }}
        contentContainerStyle={{ paddingTop: 50 }}
      >
        <Image source={logo} style={[STYLES.LOGO.IMAGE, { alignSelf: 'center' }]} />
        <View style={{ alignItems: 'center', paddingBottom: 50 }}>
          <Text style={styles.title}>Notary Login</Text>
          <Text style={styles.subtitle}>Please login to continue using our app</Text>
        </View>
        <Form style={{ flex: 1, paddingHorizontal: 20, justifyContent: 'center' }}>
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='hash' type='Feather' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Notary Number'
              placeholderTextColor='#adb4bc'
              keyboardType={'email-address'}
              returnKeyType='next'
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={value => this.onChangeText('username', value)}
            />
          </Item>
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='lock' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Password'
              placeholderTextColor='#adb4bc'
              returnKeyType='go'
              autoCapitalize='none'
              autoCorrect={false}
              secureTextEntry={true}
              onChangeText={value => this.onChangeText('password', value)}
            />
          </Item>
          <Button
            onPress={() => this.signIn()}
            loading={this.state.loading}
            text='Sign In'
            buttonStyle={STYLES.BUTTON.AUTH_SCREEN.BUTTON}
            buttonTextStyle={STYLES.BUTTON.AUTH_SCREEN.TEXT}
          />
          <View style={styles.footer}>
            <Text
              style={styles.buttonLink}
              onPress={() => this.props.navigation.navigate('NotarySignUp')}
            >
              Register
            </Text>
            <Text
              style={styles.buttonLink}
              onPress={() => this.props.navigation.navigate('NotaryForgetPassword')}
            >
              Forget Password?
            </Text>
          </View>
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  infoContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    height: 'auto',
    bottom: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    backgroundColor: '#ffffff',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 28,
    // fontFamily: 'sans-serif-thin',
  },
  subtitle: {
    color: '#888',
  },
  itemStyle: {
    marginBottom: 20,
  },
  buttonLink: {
    color: 'black',
    textDecorationLine: 'underline',
  },
  footer: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
