import React from 'react';
import { StyleSheet, View, Image } from 'react-native';

import { Text, Form } from 'native-base';

import Button from '@/components/ButtonComponent';
import { NavigationScreenProps } from 'react-navigation';

// Load the app logo
import logo from '../../assets/images/logo.png';
import { STYLES } from '@/themes';

// Amplify imports and config
import Amplify from '@aws-amplify/core';
import { NOTARY_AWS_CONFIG, DOCUMENT_OWNER_AWS_CONFIG } from '../../aws-exports';

export default class WelcomeScreen extends React.Component<NavigationScreenProps> {
  static navigationOptions = { header: null };

  gotoNotary = () => {
    Amplify.configure(NOTARY_AWS_CONFIG);
    this.props.navigation.navigate('Notary');
  }

  gotoDocumentOwner = () => {
    Amplify.configure(DOCUMENT_OWNER_AWS_CONFIG);
    this.props.navigation.navigate('Client');
  }

  render() {
    return (
      <View style={{ flexGrow: 1, paddingTop: 50 }}>
        <Image source={logo} style={[STYLES.LOGO.IMAGE, { alignSelf: 'center' }]} />
        <View style={{ alignItems: 'center' }}>
          <Text style={styles.title}>E-Notary</Text>
          <Text style={styles.subtitle}>Notarized Document Retrieval Company</Text>
          <Text style={styles.tagline}>We made your Notarized Documents Available to You on the Spot</Text>
        </View>
        <Form style={{ flex: 1, paddingHorizontal: 20, justifyContent: 'center' }}>
          <Button
            onPress={this.gotoNotary}
            text='Notary'
            buttonStyle={STYLES.BUTTON.AUTH_SCREEN.BUTTON}
            buttonTextStyle={STYLES.BUTTON.AUTH_SCREEN.TEXT}
          />
          <Button
            onPress={this.gotoDocumentOwner}
            text='Document Owner'
            buttonStyle={STYLES.BUTTON.AUTH_SCREEN.BUTTON}
            buttonTextStyle={STYLES.BUTTON.AUTH_SCREEN.TEXT}
          />
        </Form>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  infoContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    height: 'auto',
    bottom: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    backgroundColor: '#ffffff',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 28,
  },
  subtitle: {
    color: '#888',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  tagline: {
    color: '#888',
    fontSize: 10,
  },
  itemStyle: {
    marginBottom: 20,
  },
  buttonLink: {
    color: 'black',
    textDecorationLine: 'underline',
  },
  footer: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
