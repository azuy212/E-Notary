import React, { Component } from 'react';
import {
  KeyboardAvoidingView,
  StyleSheet,
  ScrollView,
  Clipboard,
  Linking,
  View,
  BackHandler,
  Alert,
  ToastAndroid,
} from 'react-native';
import { Card, CardItem, Text, H3, Container, Content, Icon } from 'native-base';

import AddPaymentScreen from './AddPaymentScreen';
import { NavigationScreenProps } from 'react-navigation';
import { getDownloadAbleUrl } from '@/app/client/services/scans';
import StepperAction from '@/components/StepperAction';

interface IState {
  submitted: boolean;
  url: string;
  error: string;
}

export default class Payment extends Component<NavigationScreenProps, IState> {
  state = {
    submitted: false,
    url: '',
    error: '',
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backEventHandler);
  }

  private backEventHandler = () => {
    if (this.state.submitted) {
      Alert.alert('Are you sure ?', 'It will discard your changes. Continue?', [
        { text: 'Discard', onPress: () => this.props.navigation.goBack(), style: 'destructive' },
        { text: 'No', style: 'cancel' },
      ]);
      return true;
    }
    return false;
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backEventHandler);
  }

  setSubmitted = (submitted: boolean) => {
    this.setState({ submitted });
    if (submitted) {
      this.generateURL();
    }
  }

  generateURL = async () => {
    try {
      const url = await getDownloadAbleUrl(this.props.navigation.getParam('key'));
      this.setState({ url });
    } catch (err) {
      this.setState({ error: err.message ? err.message : err });
    }
  }

  render() {
    const amount = 299;
    return (
      <Container>
        <Content contentContainerStyle={{ flex: 1 }}>
          <KeyboardAvoidingView
            behavior='padding'
            style={styles.container}
            enabled={true}
            keyboardVerticalOffset={20}
          >
            <Card style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <CardItem header={true}>
                <H3 style={styles.amount}>Document: {this.props.navigation.getParam('key')}</H3>
              </CardItem>
              {!this.state.submitted ? (
                <CardItem header={true} style={{ width: '100%', justifyContent: 'space-around' }}>
                  <Text note={true}>Amount Payable</Text>
                  <H3 style={styles.amount}>${(amount / 100).toFixed(2)}</H3>
                </CardItem>
              ) : (
                <H3 style={{ fontWeight: 'bold' }}>URL Generated Successfully</H3>
              )}
              <CardItem cardBody={true}>
                <AddPaymentScreen
                  {...this.props}
                  submitted={this.state.submitted}
                  setSubmitted={this.setSubmitted}
                />
              </CardItem>
              {this.state.submitted ? (
                this.state.url ? (
                  <>
                    <CardItem style={{ flex: 0.4, flexDirection: 'column' }}>
                        <Text style={{ fontWeight: 'bold' }}>This URL is valid for 10 minutes</Text>
                        <ScrollView style={{ flex: 1, height: 40 }}>
                        <Text note={true} selectable={true} style={{ textAlign: 'center' }}>
                          {this.state.url}
                        </Text>
                        </ScrollView>
                    </CardItem>
                    <CardItem
                      footer={true}
                      style={{ width: '100%', justifyContent: 'space-between' }}
                    >
                      <StepperAction
                        text='Copy URL'
                        position='left'
                        icon='md-clipboard'
                        handler={() => {
                          Clipboard.setString(this.state.url);
                          ToastAndroid.show('URL copied to clipboard', ToastAndroid.SHORT);
                        }}
                      />
                      <StepperAction
                        text='Open URL'
                        position='right'
                        icon='md-open'
                        handler={() => Linking.openURL(this.state.url)}
                      />
                    </CardItem>
                  </>
                ) : (
                  <>
                    <H3 style={{ color: '#c22', fontWeight: 'bold' }}>Error generating URL</H3>
                    <View style={styles.alertWrapper}>
                      <View style={styles.alertIconWrapper}>
                        <Icon
                          type='FontAwesome'
                          name='exclamation-circle'
                          fontSize={20}
                          style={{ color: '#c22' }}
                        />
                      </View>
                      <View style={styles.alertTextWrapper}>
                        <Text style={styles.alertText}>{this.state.error}</Text>
                      </View>
                    </View>
                    <View style={{ marginTop: 10 }}>
                      <StepperAction
                        text='Retry'
                        position='left'
                        icon='md-refresh'
                        handler={this.generateURL}
                      />
                    </View>
                  </>
                )
              ) : null}
            </Card>
          </KeyboardAvoidingView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  amount: {
    fontWeight: 'bold',
  },
  button: {
    alignSelf: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
  },
  alertTextWrapper: {
    flex: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertIconWrapper: {
    padding: 5,
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertText: {
    color: '#c22',
    fontSize: 16,
    fontWeight: '400',
  },
  alertWrapper: {
    backgroundColor: '#ecb7b7',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 5,
    paddingVertical: 5,
    marginTop: 10,
  },
});
