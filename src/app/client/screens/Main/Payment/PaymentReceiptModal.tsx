import React from 'react';
import { View, Modal } from 'react-native';
import WebView from 'react-native-webview';

interface IProps {
  uri: string;
  modalOpen: boolean;
  setModalOpen: (modal: boolean) => void;
}

const PaymentReceiptModal = ({ uri, modalOpen, setModalOpen }: IProps) => {
  return (
    <Modal
      animationType='slide'
      transparent={false}
      visible={modalOpen}
      onRequestClose={() => setModalOpen(false)}
    >
      <View style={{ flex: 1 }}>
        <WebView source={{ uri }} style={{ marginTop: 20 }} />
      </View>
    </Modal>
  );
};

export default PaymentReceiptModal;
