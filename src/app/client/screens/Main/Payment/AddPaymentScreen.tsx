import React from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { CreditCardInput } from 'react-native-credit-card-input';
import { FontAwesome } from '@expo/vector-icons';

import PaymentReceiptModal from './PaymentReceiptModal';
import { getCreditCardToken, makePayment, makePaymentAlert } from '@/app/notary/services/payment';
import ButtonComponent from '@/components/ButtonComponent';
import { NavigationScreenProps } from 'react-navigation';

const STRIPE_ERROR = 'Payment service error. Try again later.';

interface IState {
  loading: boolean;
  modalOpen: boolean;
  error: string | null;
  cardData: ICardData;
  paymentReceiptURL: string;
}

interface IProps extends NavigationScreenProps {
  submitted: boolean;
  setSubmitted: (submitted: boolean) => void;
}

export default class AddPaymentScreen extends React.Component<IProps, IState> {
  state = {
    loading: false,
    modalOpen: false,
    error: null,
    cardData: {
      status: {
        cvc: 'incomplete',
        expiry: 'incomplete',
        name: 'incomplete',
        number: 'incomplete',
      },
      valid: false,
      values: {
        cvc: '',
        expiry: '',
        name: '',
        number: '',
        type: undefined,
      },
    },
    paymentReceiptURL: '',
  } as IState;

  componentDidMount() {
    if (this.state.paymentReceiptURL) {
      this.props.setSubmitted(true);
    }
  }

  setModalOpen = () => {
    this.setState({ modalOpen: false });
  }

  setCardData = (cardData: ICardData) => {
    this.setState({
      cardData,
    });
  }

  onSubmit = async (creditCardInput: ICardData) => {
    const amount = 299;
    makePaymentAlert(amount, async () => {
      this.setState({ loading: true, error: null });
      try {
        const token = await getCreditCardToken(creditCardInput);
        const paymentResponse = await makePayment(token, amount);
        if (paymentResponse.status === 'succeeded') {
          this.setState({
            loading: false,
            paymentReceiptURL: paymentResponse.receipt_url,
          });
          this.props.setSubmitted(true);
        }
      } catch (e) {
        console.log('Error', e);
        this.setState({
          loading: false,
          error: `${STRIPE_ERROR} Error Message: ${e.message}`,
        });
        this.props.setSubmitted(false);
        return;
      }
    });
  }

  render() {
    const { error, modalOpen, loading, cardData } = this.state;
    const { submitted } = this.props;
    return (
      <>
        <PaymentReceiptModal
          uri={this.state.paymentReceiptURL}
          modalOpen={modalOpen}
          setModalOpen={this.setModalOpen}
        />
        <ScrollView style={styles.container}>
          <View style={styles.cardFormWrapper}>
            {!submitted && (
              <CreditCardInput
                additionalInputsProps={{
                  number: { editable: !submitted },
                  cvc: { editable: !submitted },
                  expiry: { editable: !submitted },
                  name: { editable: !submitted },
                }}
                allowScroll={true}
                requiresName={true}
                onChange={cardData => this.setCardData(cardData)}
              />
            )}
            <View style={styles.buttonWrapper}>
              {submitted && (
                <View
                  style={[styles.alertWrapper, { backgroundColor: 'seagreen', marginBottom: 10 }]}
                >
                  <View style={styles.alertIconWrapper}>
                    <FontAwesome name='check' size={20} style={{ color: 'lightgreen' }} />
                  </View>
                  <View style={styles.alertTextWrapper}>
                    <Text style={[styles.alertText, { color: 'white' }]}>Payment Successful!</Text>
                  </View>
                </View>
              )}
              <ButtonComponent
                text={!submitted ? 'Make Payment' : 'Show Receipt'}
                loading={loading}
                onPress={() =>
                  cardData.valid && !loading
                    ? !submitted
                      ? this.onSubmit(cardData)
                      : this.setState({ modalOpen: true })
                    : null
                }
                buttonStyle={[
                  styles.button,
                  { backgroundColor: cardData.valid ? 'black' : 'gray' },
                ]}
                buttonTextStyle={styles.buttonText}
              />
              {/* Show errors */}
              {error && (
                <View style={styles.alertWrapper}>
                  <View style={styles.alertIconWrapper}>
                    <FontAwesome name='exclamation-circle' size={20} style={{ color: '#c22' }} />
                  </View>
                  <View style={styles.alertTextWrapper}>
                    <Text style={styles.alertText}>{error}</Text>
                  </View>
                </View>
              )}
            </View>
          </View>
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textWrapper: {
    margin: 10,
  },
  infoText: {
    fontSize: 18,
    textAlign: 'center',
  },
  cardFormWrapper: {
    padding: 10,
    margin: 10,
  },
  buttonWrapper: {
    padding: 10,
    zIndex: 100,
  },
  alertTextWrapper: {
    flex: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertIconWrapper: {
    padding: 5,
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertText: {
    color: '#c22',
    fontSize: 16,
    fontWeight: '400',
  },
  alertWrapper: {
    backgroundColor: '#ecb7b7',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 5,
    paddingVertical: 5,
    marginTop: 10,
  },
  button: {
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
