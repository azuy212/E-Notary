import { ActionSheet } from 'native-base';
import { Alert, SectionListData } from 'react-native';
import { IRawScanData } from '@/model/scan';
import { bytesConverter } from '@/services/common';

type IASButtons = {
  text: string;
  icon?: string;
  iconColor?: string;
};

const ACTION_SHEET_BUTTONS: IASButtons[] = [
  { text: 'Download', icon: 'md-cloud-download', iconColor: '#1dadf5' },
  { text: 'Details', icon: 'ios-information-circle-outline' },
  { text: 'Cancel', icon: 'close', iconColor: '#25de5b' },
];

const ScanListActionSheet = (
  scan: IRawScanData,
  section: SectionListData<any>,
  showPaymentDialog: (scan: IRawScanData) => void,
) => {
  ActionSheet.show(
    {
      options: ACTION_SHEET_BUTTONS,
      cancelButtonIndex: 2,
    },
    async (index) => {
      const title = ACTION_SHEET_BUTTONS[index].text;
      switch (index) {
        case 0:
          showPaymentDialog(scan);
          break;
        case 1:
          Alert.alert(
            title,
            `
Label: ${scan.key}
Size: ${bytesConverter(scan.size!)}
Last Modified: ${section.date} ${scan.lastModified}
          `,
          );
          break;
        default:
          break;
      }
    },
  );
};

export default ScanListActionSheet;
