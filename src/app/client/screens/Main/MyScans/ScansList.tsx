import React from 'react';
import { SectionList, StyleSheet, SectionListData } from 'react-native';
import { ListItem, Text } from 'native-base';

import { IScanData, IRawScanData } from '@/model/scan';
import Loading from '@/components/Loading';
import ScansListItem from './ScansListItem';

interface IProps {
  scans: IScanData;
  refreshing: boolean;
  ready: boolean;
  listRefreshHandler: () => void;
  showPaymentDialog: (scan: IRawScanData) => void;
}

interface ISectionData {
  section: SectionListData<IScanData>;
}

const ScansList = (props: IProps) => {
  const sectionHeader = ({ section: { date } }: ISectionData) => (
    <ListItem itemHeader={true}>
      <Text style={styles.listItemHeader}>{date}</Text>
    </ListItem>
  );

  const { scans, refreshing, listRefreshHandler, ready, showPaymentDialog } = props;
  return (
    <SectionList
      renderItem={({ item, section }) => (
        <ScansListItem item={item} section={section} showPaymentDialog={showPaymentDialog} />
      )}
      renderSectionHeader={sectionHeader}
      sections={scans}
      keyExtractor={item => item.id}
      refreshing={refreshing}
      onRefresh={listRefreshHandler}
      ListEmptyComponent={() =>
        ready ? (
          <Text style={styles.noData} note={true}>
            No data available.
          </Text>
        ) : (
          <Loading />
        )
      }
    />
  );
};

export default ScansList;

const styles = StyleSheet.create({
  listItemHeader: {
    fontWeight: 'bold',
  },
  noData: {
    alignSelf: 'center',
  },
});
