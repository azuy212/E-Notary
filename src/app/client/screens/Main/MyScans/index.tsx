import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content, Root } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';

import { getScansByDate } from '@/services/common';
import { IRawScanData } from '@/model/scan';
import MyHeader from '@/components/MyHeader';

import ScansList from './ScansList';
import { showErrorAlert } from '@/services/error';
import { getClientScansFromCloud } from '@/app/client/services/scans';
import { Auth } from 'aws-amplify';

interface IState {
  ready: boolean;
  scans: IRawScanData[];
  filteredScans: IRawScanData[];
  showPaymentDialog: boolean;
  refreshing: boolean;
}

type IProps = NavigationScreenProps;

export default class MyScans extends Component<IProps, IState> {
  state = {
    ready: false,
    scans: [],
    filteredScans: [],
    showPaymentDialog: false,
    refreshing: false,
  } as IState;

  async componentDidMount() {
    this.props.navigation.addListener('didFocus', () => {
      this.setState({ refreshing: true });
      this.fetchScans();
    });
  }

  fetchScans = async () => {
    try {
      const user = await Auth.currentAuthenticatedUser();
      const userAttributes = await Auth.userAttributes(user);
      const attributesObj = userAttributes.reduce((acc, curr) => {
        acc[curr.getName()] = curr.getValue();
        return acc;
      },                                          {} as Record<string, any>);
      const scans = await getClientScansFromCloud(attributesObj.email);
      this.setState({
        scans,
        filteredScans: scans,
        refreshing: false,
        ready: true,
      });
    } catch (error) {
      showErrorAlert('Error Fetching Scans', error);
    }
  }

  showPaymentDialog = (scan: IRawScanData) => {
    this.props.navigation.navigate('Payment', scan);
  }

  handleSearchTextChange = (text: string) => {
    const filteredScans = this.state.scans.filter(value =>
      value.key.toLowerCase().includes(text.toLowerCase()),
    );
    this.setState({ filteredScans });
  }

  handleListRefresh = () => {
    this.setState({ refreshing: true });
    this.fetchScans();
  }

  handleFabIconPress = () => {
    this.props.navigation.navigate('NotarizeScan');
  }

  render() {
    const { ready, refreshing, filteredScans } = this.state;
    return (
      <Root>
        <Container>
          <MyHeader
            onSearchTextChange={this.handleSearchTextChange}
            navigateTo='ClientAuthLoading'
            {...this.props}
          />
          <Content contentContainerStyle={styles.listContainer}>
            <ScansList
              listRefreshHandler={this.handleListRefresh}
              scans={getScansByDate(filteredScans)}
              ready={ready}
              refreshing={refreshing}
              showPaymentDialog={this.showPaymentDialog}
            />
          </Content>
        </Container>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    flex: 1,
  },
  fab: {
    backgroundColor: 'black',
  },
  fabIcon: {
    fontSize: 30,
  },
});
