import { createStackNavigator } from 'react-navigation';
import MyScans from './MyScans';
import Payment from './Payment';

const ClientMainAppNavigator = createStackNavigator(
  {
    Home: { screen: MyScans },
    Payment: { screen: Payment },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
  },
);

export default ClientMainAppNavigator;
