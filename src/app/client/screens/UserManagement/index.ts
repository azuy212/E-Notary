import { createStackNavigator } from 'react-navigation';

import ClientSignInScreen from './SignInScreen';
import ClientSignUpScreen from './SignUpScreen';
import ClientForgetPasswordScreen from './ForgetPasswordScreen';

const ClientAuthStack = createStackNavigator({
  ClientSignIn: {
    screen: ClientSignInScreen,
    navigationOptions: () => ({
      title: 'Log in to Document Owner Account',
    }),
  },
  ClientSignUp: {
    screen: ClientSignUpScreen,
    navigationOptions: () => ({
      title: 'New Document Owner Account',
    }),
  },
  ClientForgetPassword: {
    screen: ClientForgetPasswordScreen,
    navigationOptions: () => ({
      title: 'Forgot your Password?',
    }),
  },
});

export default ClientAuthStack;
