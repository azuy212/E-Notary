import React from 'react';
import { StyleSheet, Alert, Image, Dimensions, Platform } from 'react-native';

import { Item, Input, Icon, Form, Text } from 'native-base';

// AWS Amplify
import Auth from '@aws-amplify/auth';

import Button from '@/components/ButtonComponent';

import Dialog from 'react-native-dialog';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// Load the app logo
import logo from '../../../../../assets/images/logo.png';
import { NavigationScreenProps } from 'react-navigation';
import { STYLES } from '@/themes';
import { handleConfirmSignUpDialog } from '@/services/userManagement';
import { showErrorAlert } from '@/services/error';

const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 2;

interface State {
  clientName: string;
  username: string;
  password: string;
  confirmPassword: string;
  modalVisible: boolean;
  authCode: string;
  loading: boolean;
  confirmCodeDialog: boolean;
}

type StateKeys = keyof State;

export default class SignUpScreen extends React.Component<NavigationScreenProps, State> {
  state = {
    clientName: '',
    username: '',
    password: '',
    confirmPassword: '',
    modalVisible: false,
    authCode: '',
    loading: false,
    confirmCodeDialog: false,
  } as State;
  // Get user input
  onChangeText(key: StateKeys, value: any) {
    this.setState({
      [key]: value,
    } as Pick<State, StateKeys>);
  }

  showModal(modalVisible: boolean) {
    this.setState({ modalVisible });
  }

  // Sign up user with AWS Amplify Auth
  async signUp() {
    const { clientName, username, password, confirmPassword } = this.state;
    // rename variable to conform with Amplify Auth field phone attribute
    this.setState({ loading: true });
    try {
      if (!username || !clientName || !password || !confirmPassword) {
        throw new Error('Please fill all fields');
      } else if (password !== confirmPassword) {
        throw new Error("Password doesn't match");
      } else {
        await Auth.signUp({
          username,
          password,
          attributes: {
            name: clientName,
          },
        });
        this.setState({ loading: false, confirmCodeDialog: true });
      }
    } catch (error) {
      this.setState({ loading: false });
      const err = error.message ? error.message : error;
      showErrorAlert('Error when signing up: ', err);
    }
  }
  // Confirm users and redirect them to the SignIn page
  async confirmSignUp() {
    const { username, authCode } = this.state;
    await Auth.confirmSignUp(username, authCode)
      .then(() => {
        this.setState({ confirmCodeDialog: false });
        Alert.alert(
          'Account Verified!',
          'Congratulations! Your account has been verified successfully, Please Sign In to your account',
          [
            {
              text: 'Sign In',
              onPress: () => this.props.navigation.navigate('ClientSignIn'),
            },
          ],
        );
      })
      .catch(err => showErrorAlert('Error when entering confirmation code: ', err));
  }
  // Resend code if not received already
  async resendSignUp() {
    const { username } = this.state;
    await Auth.resendSignUp(username)
      .then(() => {
        Alert.alert('Confirmation code resent successfully');
      })
      .catch(err => showErrorAlert('Error requesting new confirmation code: ', err));
  }

  hideConfirmCodeDialog = () => {
    this.setState({ confirmCodeDialog: false });
  }

  render() {
    return (
      <KeyboardAwareScrollView
        enableAutomaticScroll={true}
        extraScrollHeight={100}
        enableOnAndroid={true}
        extraHeight={Platform.select({ android: 100 })}
        style={{ flexGrow: 1 }}
      >
        <Image source={logo} style={[STYLES.LOGO.IMAGE, { alignSelf: 'center' }]} />
        <Form style={{ flex: 1, paddingHorizontal: 20 }}>
          {/* Notary Name section  */}
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='ios-person' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Name'
              placeholderTextColor='#adb4bc'
              onChangeText={value => this.onChangeText('clientName', value)}
            />
          </Item>
          {/* email section */}
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='email' type='Entypo' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Email*'
              placeholderTextColor='#adb4bc'
              keyboardType={'email-address'}
              autoCapitalize='none'
              onChangeText={value => this.onChangeText('username', value)}
            />
          </Item>
          {/*  password section  */}
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='lock' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Password'
              placeholderTextColor='#adb4bc'
              secureTextEntry={true}
              onChangeText={value => this.onChangeText('password', value)}
            />
          </Item>
          <Item rounded={true} style={styles.itemStyle}>
            <Icon active={true} name='lock' style={STYLES.INPUT.ICON} />
            <Input
              style={STYLES.INPUT.TEXT}
              placeholder='Confirm Password'
              placeholderTextColor='#adb4bc'
              secureTextEntry={true}
              onChangeText={value => this.onChangeText('confirmPassword', value)}
            />
          </Item>
          <Text style={{ fontSize: 10 }}>
            * Please provide the email used while notarizing the document
          </Text>
          <Button
            onPress={() => this.signUp()}
            loading={this.state.loading}
            text='Sign Up'
            buttonStyle={[STYLES.BUTTON.AUTH_SCREEN.BUTTON, { marginTop: 20 }]}
            buttonTextStyle={STYLES.BUTTON.AUTH_SCREEN.TEXT}
          />
          <Dialog.Container visible={this.state.confirmCodeDialog}>
            <Dialog.Title>Enter Confirmation Code</Dialog.Title>
            <Dialog.Description>
              Verification Code has been sent to the following email
            </Dialog.Description>
            <Dialog.Description style={{ fontWeight: 'bold' }}>
              {this.state.username}
            </Dialog.Description>
            <Dialog.Description>
              Please check your email and provide verification code
            </Dialog.Description>
            <Dialog.Input
              label='Verification Code'
              onChangeText={value => this.onChangeText('authCode', value)}
              style={{
                borderBottomColor: '#ccc',
                borderBottomWidth: 2,
              }}
            />
            <Dialog.Button label='Resend Code' onPress={() => this.resendSignUp()} />
            <Dialog.Button
              label='Cancel'
              onPress={() => handleConfirmSignUpDialog(this.hideConfirmCodeDialog)}
            />
            <Dialog.Button label='Verify' onPress={() => this.confirmSignUp()} />
          </Dialog.Container>
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemStyle: {
    marginBottom: 10,
  },
  iconStyle: {
    color: '#5a52a5',
    fontSize: 28,
    marginLeft: 15,
  },
  textStyle: {
    padding: 5,
    fontSize: 18,
  },
  countryStyle: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopColor: '#211f',
    borderTopWidth: 1,
    padding: 12,
  },
  closeButtonStyle: {
    flex: 1,
    padding: 12,
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#211f',
    backgroundColor: '#fff3',
  },
  buttonLink: {
    color: 'blue',
    textDecorationLine: 'underline',
  },
});
