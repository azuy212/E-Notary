import { IRawScanData } from '@/model/scan';

const API_BASE_URL = 'https://pk8v6dpt67.execute-api.us-east-1.amazonaws.com/dev';

export async function getClientScansFromCloud(email: string): Promise<IRawScanData[]> {
  const response = await (await fetch(`${API_BASE_URL}/getClientScans?email=${email}`, {

  })).json();
  if (response.Items) {
    const apiResponse = response.Items;
    if (apiResponse) {
      const rawScanData: IRawScanData[] = apiResponse.map((data: any) => ({
        id: data!.id,
        key: data!.documentName,
        lastModified: data!.dateTime,
        size: data!.size!,
      }));

      return rawScanData.sort(
        (a, b) => new Date(b.lastModified).getTime() - new Date(a.lastModified).getTime(),
      );
    }
  }
  return [];
}

export async function getDownloadAbleUrl(key: string) {
  const { url }: { url: string } = await (await fetch(`${API_BASE_URL}/generateS3URL?documentKey=public/${key}`)).json();
  return url;
}
