import { createSwitchNavigator } from 'react-navigation';

import ClientMainAppNavigator from './screens/Main';
import ClientAuthLoading from './screens/AuthLoading';
import ClientAuthNavigator from './screens/UserManagement';

const ClientNavigator = createSwitchNavigator({
  ClientAuthLoading,
  ClientAuth: ClientAuthNavigator,
  ClientMain: ClientMainAppNavigator,
});

export default ClientNavigator;
