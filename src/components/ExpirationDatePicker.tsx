import React from 'react';
import { DatePicker } from 'native-base';
import { getDateString, getStringDate } from '@/services/common';

interface IProps {
  placeHolder?: string;
  selectedDate: string;
  onDateSelected: (date: Date) => void;
}

const ExpirationDatePicker = (props: IProps) => {
  return (
    <DatePicker
      defaultDate={getStringDate(props.selectedDate)}
      animationType={'fade'}
      androidMode={'default'}
      placeHolderText={props.placeHolder || 'Select Date'}
      placeHolderTextStyle={{ color: '#adb4bc', fontWeight: 'bold', fontSize: 17 }}
      onDateChange={(date: Date) => props.onDateSelected(date)}
      formatChosenDate={(date: Date) => getDateString(date)}
    />
  );
};

export default ExpirationDatePicker;
