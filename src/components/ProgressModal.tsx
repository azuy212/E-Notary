import React from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  ProgressBarAndroid,
} from 'react-native';
import { bytesConverter, getPercentage } from '@/services/common';

interface IProps {
  uploading: boolean;
  loaded: number;
  total: number;
}

const ProgressModal = (props: IProps) => {
  const { uploading, loaded, total } = props;

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={uploading}
      onRequestClose={() => {
        console.log('close modal');
      }}
    >
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <Text style={styles.title}>Uploading...</Text>
          <Text>
            {bytesConverter(loaded)} / {bytesConverter(total)} (
            {getPercentage(loaded, total)} %)
          </Text>
          <ProgressBarAndroid
            style={{ width: 200 }}
            styleAttr='Horizontal'
            indeterminate={false}
            progress={total ? loaded / total : 0}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 150,
    width: 300,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
  },
});

export default ProgressModal;
