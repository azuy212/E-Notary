import React, { Component } from 'react';
import { Icon, Header, Input, Item, Body, Right } from 'native-base';
import { StyleSheet, Keyboard, Alert } from 'react-native';

import Auth from '@aws-amplify/auth';
import { NavigationScreenProps } from 'react-navigation';

interface IMyProps {
  navigateTo: string;
  backListener?: () => void;
  onSearchTextChange?: (text: string) => void;
}

type IProps = Header['props'] & IMyProps & NavigationScreenProps;

interface IState {
  search: string;
}

export default class MyHeader extends Component<IProps, IState> {
  state = {
    search: '',
  };
  render() {
    return (
      <Header transparent={true} searchBar={true} style={styles.header} {...this.props}>
        <Body style={{ flex: 8 }}>
          <Item rounded={true}>
            <Icon name='md-search' onPress={this.props.backListener} />
            <Input placeholder='Search File' onChangeText={this.props.onSearchTextChange} />
            {this.state.search ? (
              <Icon
                name='md-close'
                onPress={() => {
                  this.setState({ search: '' });
                  Keyboard.dismiss();
                }}
              />
            ) : null}
          </Item>
        </Body>
        <Right style={{ flex: 1 }}>
          <Icon
            name='sign-out'
            type='Octicons'
            onPress={() => {
              Alert.alert('Sign Out', 'Do you really want to Sign Out?', [
                {
                  text: 'Sign Out',
                  style: 'destructive',
                  onPress: async () => {
                    await Auth.signOut();
                    this.props.navigation.navigate(this.props.navigateTo);
                  },
                },
                { text: 'Cancel', style: 'cancel' },
              ]);
            }}
          />
        </Right>
      </Header>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    margin: 10,
  },
});
