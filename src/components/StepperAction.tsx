import React from 'react';
import { Button, Icon, Text } from 'native-base';
import { StyleSheet } from 'react-native';

type IProps = {
  position: 'left' | 'right';
  handler: () => void;
  icon: string;
  text: string;
};

const StepperAction = (props: IProps) => {
  const { position, handler, icon, text } = props;
  return (
    <Button
      transparent={true}
      bordered={true}
      iconLeft={position === 'left'}
      iconRight={position === 'right'}
      dark={true}
      style={styles.button}
      onPress={handler}
    >
      {position === 'left' ? (
        <>
          <Icon name={icon} />
          <Text>{text}</Text>
        </>
      ) : (
        <>
          <Text>{text}</Text>
          <Icon name={icon} />
        </>
      )}
    </Button>
  );
};

export default StepperAction;

const styles = StyleSheet.create({
  button: {
    alignSelf: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
  },
});
