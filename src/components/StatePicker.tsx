import React from 'react';
import { Icon, Picker } from 'native-base';

import { US_STATES } from '@/services/common';
import { US_STATE_KEYS } from '@/app/notary/model/states';

interface IProps {
  selectedState: US_STATE_KEYS;
  onStateSelected: (value: US_STATE_KEYS) => void;
}

const StatePicker = (props: IProps) => {
  return (
    <Picker
      mode='dropdown'
      iosIcon={<Icon name='arrow-down' />}
      style={{ width: undefined }}
      placeholderStyle={{ color: '#bfc6ea' }}
      placeholderIconColor='#007aff'
      selectedValue={props.selectedState}
      onValueChange={props.onStateSelected}
    >
      {Object.entries(US_STATES).map(value => (
        <Picker.Item label={value[1]} value={value[0]} key={value[0]} />
      ))}
    </Picker>
  );
};

export default StatePicker;
