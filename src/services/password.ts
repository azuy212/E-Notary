export interface IStrengthTest {
  func: (arg0: string) => boolean;
  description: string;
}

export const strengthTests: { [key: string]: IStrengthTest } = {
  length: {
    func: (v: string) => v.length >= 6,
    description: 'Minimum 6 characters',
  },
  lowercase: {
    func: (v: string) => /[a-z]/.test(v),
    description: 'One lowercase letter',
  },
  uppercase: {
    func: (v: string) => /[A-Z]/.test(v),
    description: 'One uppercase letter',
  },
  number: {
    func: (v: string) => /\d/.test(v),
    description: 'One number',
  },
  symbol: {
    func: (v: string) => /[^0-9a-zA-Zs]/.test(v),
    description: 'One symbol',
  },
};

const password = {
  getMessage(value: string) {
    return Object.values(strengthTests).find(rule => !rule.func(value))?.description;
  },

  validate: (value: string) => {
    return Object.values(strengthTests).reduce(
      (valid: boolean, rule) => valid && rule.func(value),
      true,
    );
  },
};

export default password;
