import { Alert } from 'react-native';

export const signOutAlert = async (signOut: () => void) => {
  Alert.alert(
    'Sign Out',
    'Are you sure you want to sign out from the app?',
    [
      {
        text: 'Cancel',
        onPress: () => console.log('Canceled'),
        style: 'cancel',
      },
      { text: 'OK', onPress: signOut },
    ],
    { cancelable: false },
  );
};

export const handleConfirmSignUpDialog = (hideConfirmCodeDialog: () => void) => {
  Alert.alert(
    'Are you sure?',
    `You cannot verify your account later by yourself,
     You need to contact administrator for account verification`,
    [
      {
        text: 'Verify Later',
        onPress: hideConfirmCodeDialog,
        style: 'destructive',
      },
      {
        text: 'Verify Now',
        style: 'default',
      },
    ],
  );
};
