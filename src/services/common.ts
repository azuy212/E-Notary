import moment from 'moment';
import * as Permissions from 'expo-permissions';

import { IUSStates } from '@/app/notary/model/states';
import { IScanData, IRawScanData } from '@/model/scan';

export function getScansByDate(data: IRawScanData[]) {
  return data.reduce<IScanData>((acc, curr) => {
    const dateString = moment
      .utc(curr.lastModified)
      .local()
      .format('MMMM Do, YYYY');
    let foundIndex = acc.findIndex(data => data.date === dateString);
    if (foundIndex < 0) {
      foundIndex = acc.push({ date: dateString, data: [] }) - 1;
    }
    acc[foundIndex].data.push({
      ...curr,
      lastModified: moment
        .utc(curr.lastModified)
        .local()
        .format('hh:mm a'),
    });
    return acc;
  },                            []);
}

export async function askForCameraPermissions() {
  try {
    const cameraPermission = await Permissions.askAsync(Permissions.CAMERA);
    const cameraRollPermission = await Permissions.askAsync(
      Permissions.CAMERA_ROLL,
    );
    return (
      cameraPermission.status === 'granted' &&
      cameraRollPermission.status === 'granted'
    );
  } catch (error) {
    throw error;
  }
}

export function bytesConverter(bytes: number) {
  let result;
  const kBs = Math.round((bytes * 100) / 1024) / 100;
  if (kBs >= 1024) {
    const MBs = Math.round((kBs * 100) / 1024) / 100;
    result = `${MBs} MB`;
  } else {
    result = `${kBs} kB`;
  }
  return result;
}

export function getPercentage(curr: number, total: number) {
  if (total === 0) {
    return 0;
  }
  return Math.round((10000 * curr) / total) / 100;
}

export function getDateString(date: Date) {
  return moment(date).format('MM/DD/YYYY');
}

export function getStringDate(date: string) {
  return date ? moment(date, 'MM/DD/YYYY').toDate() : new Date();
}

export const US_STATES: IUSStates = {
  AL: 'Alabama',
  AK: 'Alaska',
  AS: 'American Samoa',
  AZ: 'Arizona',
  AR: 'Arkansas',
  CA: 'California',
  CO: 'Colorado',
  CT: 'Connecticut',
  DE: 'Delaware',
  DC: 'District Of Columbia',
  FM: 'Federated States Of Micronesia',
  FL: 'Florida',
  GA: 'Georgia',
  GU: 'Guam',
  HI: 'Hawaii',
  ID: 'Idaho',
  IL: 'Illinois',
  IN: 'Indiana',
  IA: 'Iowa',
  KS: 'Kansas',
  KY: 'Kentucky',
  LA: 'Louisiana',
  ME: 'Maine',
  MH: 'Marshall Islands',
  MD: 'Maryland',
  MA: 'Massachusetts',
  MI: 'Michigan',
  MN: 'Minnesota',
  MS: 'Mississippi',
  MO: 'Missouri',
  MT: 'Montana',
  NE: 'Nebraska',
  NV: 'Nevada',
  NH: 'New Hampshire',
  NJ: 'New Jersey',
  NM: 'New Mexico',
  NY: 'New York',
  NC: 'North Carolina',
  ND: 'North Dakota',
  MP: 'Northern Mariana Islands',
  OH: 'Ohio',
  OK: 'Oklahoma',
  OR: 'Oregon',
  PW: 'Palau',
  PA: 'Pennsylvania',
  PR: 'Puerto Rico',
  RI: 'Rhode Island',
  SC: 'South Carolina',
  SD: 'South Dakota',
  TN: 'Tennessee',
  TX: 'Texas',
  UT: 'Utah',
  VT: 'Vermont',
  VI: 'Virgin Islands',
  VA: 'Virginia',
  WA: 'Washington',
  WV: 'West Virginia',
  WI: 'Wisconsin',
  WY: 'Wyoming',
};
