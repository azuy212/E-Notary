export interface IRawScanData {
  id?: string;
  eTag?: string;
  key: string;
  lastModified: string;
  size?: number;
}

export interface ScanData {
  date: string;
  data: IRawScanData[];
}

export interface IScanData extends Array<ScanData> {}
