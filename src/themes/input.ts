import { StyleProp, TextStyle, Dimensions } from 'react-native';

const { height } = Dimensions.get('screen');

export const INPUT_STYLE: StyleProp<TextStyle> = {
  color: '#000',
  height: height / 15,
  flex: 1,
  fontSize: 17,
  fontWeight: 'bold',
};

export const INPUT_ICON: StyleProp<TextStyle> = {
  color: '#000',
  fontSize: 28,
  marginLeft: 15,
};
