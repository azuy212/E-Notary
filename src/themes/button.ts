import { StyleProp, ViewStyle, TextStyle, Dimensions } from 'react-native';

const { height } = Dimensions.get('screen');

export const AUTH_SCREEN_BUTTON_STYLE: StyleProp<ViewStyle> = {
  alignItems: 'center',
  backgroundColor: '#a01430',
  padding: height / 60,
  borderRadius: 5,
};

export const AUTH_SCREEN_BUTTON_TEXT_STYLE: StyleProp<TextStyle> = {
  fontSize: 18,
  fontWeight: 'bold',
  color: '#fff',
  // fontFamily: 'sans-serif-thin',
};
