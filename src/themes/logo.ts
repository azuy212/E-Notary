import { Dimensions, StyleProp, ViewStyle, ImageStyle } from 'react-native';

const { height, width } = Dimensions.get('window');

export const LOGO_CONTAINER_STYLE: StyleProp<ViewStyle> = {
  position: 'absolute',
  left: 0,
  right: 0,
  height: 'auto',
  bottom: height * 0.6,
  alignItems: 'center',
  justifyContent: 'center',
  flex: 1,
};

export const LOGO_STYLE: StyleProp<ImageStyle> = {
  width: width / 2,
  height: width / 2,
};
