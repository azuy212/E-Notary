import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import NotaryNavigator from '@/app/notary';
import ClientNavigator from '@/app/client';
import WelcomeScreen from '@/app/WelcomeScreen';

// All Navigators
export const AllNavigators = createSwitchNavigator({
  Welcome: WelcomeScreen,
  Notary: NotaryNavigator,
  Client: ClientNavigator,
});

export default createAppContainer(AllNavigators);
